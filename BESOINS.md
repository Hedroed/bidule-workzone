# IS :)

## Besoins :

- logging léger interne et remote (si connecter au réseau)
_ logging désactivable en prod pour gagner de l'autonomie
- server web de configuration
- utilisable à distance via serveur web
- scriptable
- C : doit consommé moins de 1 ampere en charge
- C : le binaire/programme doit faire moins de 100 mo au total
- C : doit utiliser moins de 100 mo de ram
- robuste aka segmentation des processus
- garantir un accès débogage via wifi/ssh si le soft plante
