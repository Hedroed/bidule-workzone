# Cross compilation for Alpine Raspberry pi

## Install toolchain (rustup)

```
rustup default stable
rustup target add aarch64-unknown-linux-musl
```

Special config for cargo

In `~/.cargo/config`

```
[target.aarch64-unknown-linux-musl]
linker="aarch64-linux-gnu-gcc"
```

For dbus :

In `~/.cargo/config`

```
[target.aarch64-unknown-linux-musl.dbus]
rustc-link-search = [
    "/home/hedroed/tmp/wifi-connect/libraries/lib/aarch64-linux-gnu",
    "/home/hedroed/tmp/wifi-connect/libraries/usr/lib/aarch64-linux-gnu",
]

rustc-link-lib = [
    "dbus-1",
    "gcrypt",
    "gpg-error",
    "lz4",
    "lzma",
    "pcre",
    "selinux",
    "systemd",
]
```

> Building libs in `libs.zip`

## Build with specific target

```
cargo build --target aarch64-unknown-linux-musl
```

## Building with cross

```
cross build --target aarch64-unknown-linux-musl --release
```
