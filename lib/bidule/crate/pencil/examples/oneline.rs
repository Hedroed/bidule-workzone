use pencil::*;

fn main() {
    println!("Drawing one line");

    // create a canvas to draw on
    let mut canvas = Canvas::new(122, 250);
    
    // create a new drawing
    let line = components::line::Line::new(60, components::line::Direction::Vertical);

    // add it to the canvas
    canvas.display_list.add(Box::new(line));

    // save the canvas as an svg
    let buf: Vec<u8> = render_epd(canvas)
    .expect("Failed to render");

    std::fs::write("oneline.dat", buf).expect("Failed to save");
}

