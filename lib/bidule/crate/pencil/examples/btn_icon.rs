use pencil::*;
use std::string::String;

fn main() {
    println!("Drawing btn icon");

    // create a canvas to draw on
    let mut canvas = Canvas::new(250, 122);
    // let mut canvas = Canvas::new(122, 250);

    let line = components::line::Line::new(16, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));
    let line = components::line::Line::new(90, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));

    let icon = components::btn_icon::BtnIcon::new(10, 100);
    canvas.display_list.add(Box::new(icon));
    
    let icon = components::btn_icon::BtnIcon::new(50, 100);
    canvas.display_list.add(Box::new(icon));
    
    let icon = components::btn_icon::BtnIcon::new(100, 100);
    canvas.display_list.add(Box::new(icon));

    let text = components::word::Word::new(10, 3, String::from("Hello World!"), false);
    canvas.display_list.add(Box::new(text));
    
    let text = components::word::Word::new(100, 3, String::from("Le paté"), true);
    canvas.display_list.add(Box::new(text));

    // save the canvas as an svg
    let buf: Vec<u8> = render_epd(canvas)
    .expect("Failed to render");

    std::fs::write("btn_icon.dat", buf).expect("Failed to save");
}

