use std::error::Error;
use std::fmt;

use crate::Canvas;

#[derive(Debug, Clone)]
struct CanvasError;

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for CanvasError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "out of canvas bound")
    }
}

impl Error for CanvasError {}

pub struct RenderCanvas<'a> {
    pub width: usize,
    pub height: usize,
    canvas: &'a mut Vec<u8>,
}

impl RenderCanvas<'_> {
    pub fn set(&mut self, x: usize, y: usize, color: u8) -> Result<(), Box<dyn Error>> {
        if x < self.width && y < self.height {
            let pos = x + y * self.width;
            self.canvas[pos] = color;
        } else {
            println!("Out of canvas {}, {}", x, y)
        }
        Ok(())
    }

    pub fn get(&self, x: usize, y: usize) -> Result<u8, Box<dyn Error>> {
        if x < self.width && y < self.height {
            let pos = x + y * self.width;
            Ok(self.canvas[pos])
        } else {
            println!("Out of canvas {}, {}", x, y);
            Err(Box::new(CanvasError{}))
        }
    }
}

pub fn render_raw(canvas: Canvas, buffer: &mut Vec<u8>) -> Result<(), Box<dyn Error>> {
    
    for idx in 0..buffer.len() {
        buffer[idx] = 255;
    }

    let mut ret = RenderCanvas{
        width: canvas.width,
        height: canvas.height,
        canvas: buffer,
    };

    for component in canvas.display_list.components {
        component.render_epd(&mut ret)?;
    }

    Ok(())
}

pub struct dep_EPDRenderer {
    width: usize,
    height: usize,
}

impl dep_EPDRenderer {
    fn to_bytes(&self, canvas: &RenderCanvas) -> Vec<u8> {
        let linewidth: usize = {
            if self.width % 8 == 0 { self.width / 8 }
            else { self.width / 8 + 1 }
        };
        let mut buf = vec![0xffu8; linewidth * self.height]; // 16 * 250 == 4000

        // TODO: Orientation (Portait vs Paysage)
        for y in 0..canvas.height {
            for x in 0..canvas.width {
                let pos: usize = x + y * canvas.width; // vertical mirror
                if canvas.canvas[pos] == 1 {
                    let new_x = self.width - y - 1;
                    let new_y = self.height - x - 1;
                    let byte_pos = new_x / 8 + new_y * linewidth;
                    buf[byte_pos] &= !(0x80 >> (new_x % 8))
                }
            }
        }
        return buf;
    }
}

pub fn dep_render_epd(canvas: &Canvas) -> Result<Vec<u8>, Box<dyn Error>> {
// pub fn render_epd(buf: &mut Vec<u8>, canvas: Canvas) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut ret = RenderCanvas{
        width: canvas.width,
        height: canvas.height,
        // canvas: buf,
        canvas: &mut vec![0; canvas.width * canvas.height],
    };

    for component in canvas.display_list.components.iter() {
        component.render_epd(&mut ret)?;
    }

    let renderer = dep_EPDRenderer{width: canvas.height, height: canvas.width};
    Ok(renderer.to_bytes(&ret))
}

pub fn convert_to_epd(in_buf: &Vec<u8>, width: usize, height: usize) -> Vec<u8> {

    const CANVAS_WIDTH: usize = 250;
    const CANVAS_HEIGHT: usize = 122;

    let linewidth: usize = {
        if width % 8 == 0 { width / 8 }
        else { width / 8 + 1 }
    };
    let mut buf = vec![0xffu8; linewidth * height]; // 16 * 250 == 4000

    for y in 0..CANVAS_HEIGHT {
        for x in 0..CANVAS_WIDTH {
            let pos: usize = x + y * CANVAS_WIDTH; // vertical mirror
            if in_buf[pos] == 0 {
                let new_x = width - y - 1;
                let new_y = height - x - 1;
                let byte_pos = new_x / 8 + new_y * linewidth;
                buf[byte_pos] &= !(0x80 >> (new_x % 8))
            }
        }
    }
    return buf;
}