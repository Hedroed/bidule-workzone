use std::error::Error;
use std::fmt;
use std::include_bytes;
use std::convert::TryInto;
use crate::epdrenderer::RenderCanvas;

static FONT_DEFAULT_NORMAL_DATA: &'static [u8] = include_bytes!("font_default_normal.bin");

static FONT_DEFAULT_BOLD_DATA: &'static [u8] = include_bytes!("font_default_bold.bin");

pub struct Font {
    data: &'static[u8],
    width: usize,
    height: usize,
}

pub static FONT_DEFAULT_NORMAL: &'static Font = &Font {data: FONT_DEFAULT_NORMAL_DATA, width: 5, height: 10};

pub static FONT_DEFAULT_BOLD: &'static Font = &Font {data: FONT_DEFAULT_BOLD_DATA, width: 6, height: 11};


#[derive(Debug, Clone)]
struct LetterError;

// Generation of an error is completely separate from how it is displayed.
// There's no need to be concerned about cluttering complex logic with the display style.
//
// Note that we don't store any extra info about the errors. This means we can't state
// which string failed to parse without modifying our types to carry that information.
impl fmt::Display for LetterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid letter, must be ascii code lower than 128")
    }
}

impl Error for LetterError {}



fn draw_unknown(canvas: &mut RenderCanvas, x: usize, y: usize, font: &Font, color: u8) -> Result<(), Box<dyn Error>> {
    let mut b1 = true;
    for yy in 0..font.height {
        let mut b2 = b1;
        for xx in 0..font.width {
            if b2 {
                canvas.set(x + xx, y + yy, color)?;
            }
            b2 = !b2;
        }
        b1 = !b1;
    }
    Ok(())
}

pub fn draw_letter(canvas: &mut RenderCanvas, x: usize, y: usize, font: &Font, letter: Option<u8>, color: u8) -> Result<(), Box<dyn Error>> {
    match letter {
        Some(l) => draw_letter_uncheck(canvas, x, y, font, l, color),
        None => draw_unknown(canvas, x, y, font, color),
    }
}

fn draw_letter_uncheck(canvas: &mut RenderCanvas, x: usize, y: usize, font: &Font, letter: u8, color: u8) -> Result<(), Box<dyn Error>> {
    let letter_code: usize = letter.try_into().unwrap();
    if letter_code >= 128 {
        return Err(Box::new(LetterError));
    }
    
    for yy in 0..font.height {
        let code = font.data[letter_code * font.height + yy];

        for xx in 0..font.width {

            let d = (code >> xx) & 1;

            if d == 1 {
                canvas.set(x + xx, y + yy, color)?;
            }
        }
    }
    Ok(())
}