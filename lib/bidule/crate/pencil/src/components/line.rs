use std::error::Error;
use crate::epdrenderer::RenderCanvas;
use crate::components::Component;
use crate::ComponentId;

#[derive(Debug,Clone)]
pub enum Direction {
    Horizontal,
    Vertical,
}

#[derive(Debug,Clone)]
pub struct FullLine {
    coord: usize,
    dir: Direction,
}

impl FullLine {
    pub fn new(coord: usize, dir: Direction) -> FullLine {
        FullLine {coord, dir,}
    }
}

impl Component for FullLine {
    fn get_id(&self) -> ComponentId {
        return 0
    }

    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
        match self.dir {
            Direction::Horizontal => {
                for x in 0..canvas.width {
                    canvas.set(x, self.coord, 0)?;
                }
            }
            Direction::Vertical => {
                for y in 0..canvas.height {
                    canvas.set(self.coord, y, 0)?;
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug,Clone)]
pub struct Line {
    coord: usize,
    dir: Direction,
    offset: usize,
    size: usize,
}

impl Line {
    pub fn new(coord: usize, dir: Direction, offset: usize, size: usize) -> Line {
        Line {coord, dir, offset, size}
    }
}

impl Component for Line {
    fn get_id(&self) -> ComponentId {
        return 4
    }

    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
        match self.dir {
            Direction::Horizontal => {
                for x in self.offset..(self.offset+self.size) {
                    canvas.set(x, self.coord, 0)?;
                }
            }
            Direction::Vertical => {
                for y in self.offset..(self.offset+self.size) {
                    canvas.set(self.coord, y, 0)?;
                }
            }
        }
        Ok(())
    }
}

