pub mod line;
pub mod btn_icon;
pub mod word;
pub mod invert_rect;

use std::error::Error;

use crate::ComponentId;
use crate::epdrenderer::RenderCanvas;

pub trait Component: CloneComponent + Sync + Send {
    fn get_id(&self) -> usize;
    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(),Box<dyn Error>>;
}

pub trait CloneComponent {
    fn clone_component(&self) -> Box<dyn Component>;
}

impl<T> CloneComponent for T
    where T: Component + Clone + 'static {
    fn clone_component(&self) -> Box<dyn Component> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Component> {
    fn clone(&self) -> Self {
        self.clone_component()
    }
}

#[derive(Clone)]
/// A sorted Vec of Components, ordered from bottom to top.
pub struct DisplayList {
    pub components: Vec<Box<dyn Component>>,
}

impl DisplayList {
    /// Create a new empty display list
    pub fn new() -> DisplayList {
        DisplayList { components: vec![] }
    }

    /// Adds a component to the top of the display list.
    /// Returns a ComponentId handle that can be used to refer to the component in the future.
    pub fn add(&mut self, component: Box<dyn Component>) -> ComponentId {
        let child_id = self.components.len();
        self.components.push(component);
        child_id
    }

    /// Remove a component from the display list
    /// todo: Implement
    pub fn remove(&mut self, _component_id: ComponentId) {
        unimplemented!()
    }
}