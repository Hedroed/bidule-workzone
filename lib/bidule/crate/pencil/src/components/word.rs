use std::error::Error;
use crate::epdrenderer::RenderCanvas;
use crate::components::Component;
use crate::ComponentId;

use crate::font::{draw_letter, FONT_DEFAULT_NORMAL, FONT_DEFAULT_BOLD};

#[derive(Debug,Clone)]

pub struct Word {
    x: usize,
    y: usize,
    text: String,
    bold: bool,
}

impl Word {
    pub fn new(x: usize, y: usize, text: String, bold: bool) -> Word {
        Word {x, y, text, bold,}
    }
}

impl Component for Word {
    fn get_id(&self) -> ComponentId {
        return 2
    }

    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
        let (font, size) = {
            if self.bold {
                (FONT_DEFAULT_BOLD, 7)
            } else {
                (FONT_DEFAULT_NORMAL, 6)
            }
        };
        
        for (idx, byte) in self.text.as_bytes().iter().enumerate() {
            if *byte < 128 {
                draw_letter(canvas, self.x + idx * size, self.y, font, Some(*byte), 0)?;
            } else {
                draw_letter(canvas, self.x + idx * size, self.y, font, None, 0)?;
            }
        }
        Ok(())
    }
}

