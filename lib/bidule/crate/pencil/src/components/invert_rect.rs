use std::error::Error;
use crate::epdrenderer::RenderCanvas;
use crate::components::Component;
use crate::ComponentId;


#[derive(Debug,Clone)]
pub struct InvertRect {
    x: usize,
    y: usize,
    width: usize,
    height: usize,
}

impl InvertRect {
    pub fn new(x: usize, y: usize, width: usize, height: usize) -> InvertRect {
        InvertRect {x, y, width, height,}
    }
}

impl Component for InvertRect {
    fn get_id(&self) -> ComponentId {
        return 0
    }

    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
        for x in 0..self.width {
            for y in 0..self.height {
                if let Ok(color) = canvas.get(x + self.x, y + self.y) {
                    if color == 0 {
                        canvas.set(x + self.x, y + self.y, 255)?;
                    } else {
                        canvas.set(x + self.x, y + self.y, 0)?;
                    }
                }
            }
        }
        Ok(())
    }
}
