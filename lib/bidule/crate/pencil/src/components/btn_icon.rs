use std::error::Error;
use crate::epdrenderer::RenderCanvas;
use crate::components::Component;
use crate::ComponentId;

const SIZE:usize = 10;

#[derive(Debug,Clone)]
pub struct BtnIcon {
    x: usize,
    y: usize,
}

impl BtnIcon {
    pub fn new(x: usize, y: usize) -> BtnIcon {
        BtnIcon {x, y,}
    }
}

fn draw_line(x: usize, y: usize, size: usize, offset: usize, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
    for i in 0..size {
        canvas.set(x+offset+i, y, 0)?;
    }
    Ok(())
}

impl Component for BtnIcon {
    fn get_id(&self) -> ComponentId {
        return 1
    }

    fn render_epd(&self, canvas: &mut RenderCanvas) -> Result<(), Box<dyn Error>> {
        let x = self.x;
        let y = self.y;

        // Draw top & bottom lines
        for i in 0..SIZE {
            canvas.set(x + i, y, 0)?;
            canvas.set(x + i, y + SIZE - 1, 0)?;
        }
        // Draw left & right lines
        for i in 0..SIZE {
            canvas.set(x, y + i, 0)?;
            canvas.set(x + SIZE - 1, y + i, 0)?;
        }
        // Draw disc
        draw_line(x+2, y+2, 4, 1, canvas)?;
        draw_line(x+2, y+3, 6, 0, canvas)?;
        draw_line(x+2, y+4, 6, 0, canvas)?;
        draw_line(x+2, y+5, 6, 0, canvas)?;
        draw_line(x+2, y+6, 6, 0, canvas)?;
        draw_line(x+2, y+7, 4, 1, canvas)?;

        Ok(())
    }
}

