//! `pencil` is a simple 2D vector drawing library.
//!
//! - `Canvas` is a container that defines the size and top-level components of your drawing.
//! - `Drawing` defines the position, style, and sub-components of a drawing.
//! - `Shape` defines the geometry of an individual shape such as a `Circle` or `Line`.
//! - `Style` defines the fill and stroke of a drawing.  
//!
//! The general flow for creating a piece of art is:
//!
//! 1. Create a `Canvas`
//! 2. Create `Drawing` objects and add them to the Canvas `display_list`.
//! 3. Position and style the drawings
//!
//! ## Basic Example
//!
//! ```rust
//! use pencil::*;
//!
//! // create a canvas to draw on
//! let mut canvas = Canvas::new(100, 100);
//!
//! // create a new drawing
//! let mut line = line::Line {
//!     coord: 10,
//!     dir: line::Direction::horizontal,
//!     stroke: 1,
//! };
//! // add it to the canvas
//! canvas.display_list.add(line);
//!
//! // save the canvas as an svg
//! let buf: Vec<u8> = render_raw(
//!     &canvas,
//! )
//! .expect("Failed to render");
//! ```
//!
pub mod canvas;
pub mod components;
pub mod epdrenderer;
pub mod font;

pub use crate::canvas::Canvas;
pub use crate::components::*;
pub use crate::epdrenderer::render_raw;

/// Drawings are stored in a vector; this `usize` is a handle to access the child
pub type ComponentId = usize;

