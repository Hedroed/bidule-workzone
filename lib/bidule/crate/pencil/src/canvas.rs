use crate::components::DisplayList;

#[derive(Clone)]
/// Container for a drawing
pub struct Canvas {
    /// Width of the canvas in pixels
    pub width: usize,
    /// Height of the canvas in pixels
    pub height: usize,
    /// Display list; contains drawings ordered from bottom to top
    pub display_list: DisplayList,
}

impl Canvas {
    /// New empty Canvas with no background
    pub fn new(width: usize, height: usize) -> Canvas {
        Canvas {
            width,
            height,
            display_list: DisplayList::new(),
        }
    }

}

