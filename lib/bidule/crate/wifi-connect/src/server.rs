use std::sync::Arc;
use tokio;
use tokio::sync::Mutex;
use tokio::sync::mpsc::{Receiver, Sender};
use std::path::PathBuf;
use std::net::Ipv4Addr;
use std::convert::Infallible;

use warp::Filter;
use warp::http::{StatusCode, Response};

use crate::network::{NetworkCommand, NetworkConnect, NetworkCommandResponse};

struct RequestSharedState {
    gateway: Ipv4Addr,
    server_rx: Receiver<NetworkCommandResponse>,
    network_tx: Sender<NetworkCommand>,
    // exit_tx: Sender<ExitResult>,
}

// TODO: redirect to real ip / host if il's wrong
fn redirect(path: String) -> Response<()> {
    Response::builder()
        .header("Location", path)
        .status(StatusCode::MOVED_PERMANENTLY)
        .body(())
        .unwrap()
}

fn json_body() -> impl Filter<Extract = (NetworkConnect,), Error = warp::Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

fn with_state(request_state: Arc<Mutex<RequestSharedState>>) -> impl Filter<Extract = (Arc<Mutex<RequestSharedState>>,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || request_state.clone())
}

pub async fn start_server(
    gateway: Ipv4Addr,
    listening_port: u16,
    server_rx: Receiver<NetworkCommandResponse>,
    network_tx: Sender<NetworkCommand>,
    // exit_tx: Sender<ExitResult>,
    ui_directory: PathBuf,
) {
    // let exit_tx_clone = exit_tx.clone();
    let request_state = RequestSharedState {
        gateway,
        server_rx,
        network_tx,
        // exit_tx,
    };

    // let mut assets = Mount::new();
    // assets.mount("/", router);
    // assets.mount("/static", Static::new(&ui_directory.join("static")));
    // assets.mount("/css", Static::new(&ui_directory.join("css")));
    // assets.mount("/img", Static::new(&ui_directory.join("img")));
    // assets.mount("/js", Static::new(&ui_directory.join("js")));

    // let mut chain = Chain::new(assets);
    // chain.link(Write::<RequestSharedState>::both(request_state));
    // chain.link_after(RedirectMiddleware);

    let state = Arc::new(Mutex::new(request_state));

    debug!("Serving index from file {:?}", ui_directory.join("index.html"));

    let index = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file(ui_directory.join("index.html")));
    
    let dir = warp::path("static")
        .and(warp::fs::dir(ui_directory.join("static")));
    
    let networks = warp::get()
        .and(warp::path("networks"))
        .and(with_state(state.clone()))
        .and_then(networks_handler)
        .map(|reply| {
            warp::reply::with_header(reply, "Access-Control-Allow-Origin", "*")
        });

    let connect = warp::post()
        .and(warp::path("connect"))
        .and(json_body())
        .and(with_state(state))
        .and_then(connect_handler)
        .map(|reply| {
            warp::reply::with_header(reply, "Access-Control-Allow-Origin", "*")
        });

    let routes = index
        .or(dir)
        .or(networks)
        .or(connect)
        // Wrap all the routes with a filter that creates a `tracing` span for
        // each request we receive, including data about the request.
        .with(warp::trace::request());

    info!("Starting HTTP server on {}:{}", gateway, listening_port);

    warp::serve(routes).run((gateway, listening_port)).await;
}

async fn networks_handler(state: Arc<Mutex<RequestSharedState>>) -> Result<impl warp::Reply, Infallible> {
    info!("User connected to the captive portal");

    let mut request_state = state.lock().await;

    request_state.network_tx.send(NetworkCommand::Activate).await.unwrap();

    let networks = match request_state.server_rx.recv().await.unwrap() {
        NetworkCommandResponse::Networks(networks) => networks,
    };

    Ok(warp::reply::json(&networks))
}

async fn connect_handler(network: NetworkConnect, state: Arc<Mutex<RequestSharedState>>) -> Result<impl warp::Reply, Infallible> {

    debug!("Incoming `connect` to access point `{:?}` request", network);
    
    let mut request_state = state.lock().await;

    let command = NetworkCommand::Connect(network);
    request_state.network_tx.send(command).await.unwrap();

    Ok(StatusCode::OK)
}
