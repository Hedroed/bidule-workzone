// use std::env;
// use log::{LogLevel, LogLevelFilter, LogRecord};
// use env_logger::LogBuilder;

// use log::Level;
use log_subscriber::fmt;
use log_subscriber::prelude::*;

pub fn init() -> opentelemetry_jaeger::Uninstall {
    // Install a new OpenTelemetry trace pipeline
    let (tracer, _uninstall) = opentelemetry_jaeger::new_pipeline()
        .with_service_name("wifi-connect")
        .install().unwrap();

    // Create a tracing layer with the configured tracer
    let telemetry_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    let fmt_layer = fmt::layer()
        .with_target(false);

    // Use the tracing subscriber `Registry`, or any other subscriber that impls `LookupSpan`
    tracing_subscriber::registry()
    .with(telemetry_layer)
    .with(fmt_layer)
    .init();

    return _uninstall;

    // if env::var("RUST_LOG").is_ok() {
    //     builder.parse(&env::var("RUST_LOG").unwrap());


    // } else {
    //     let format = |record: &LogRecord| {
    //         if record.level() == LogLevel::Info {
    //             format!("{}", record.args())
    //         } else {
    //             format!(
    //                 "[{}:{}] {}",
    //                 record.location().module_path(),
    //                 record.level(),
    //                 record.args()
    //             )
    //         }
    //     };

    //     builder.format(format).filter(None, LogLevelFilter::Debug);

    //     builder.parse("wifi-connect=debug");
    // }

    // builder.init().unwrap();
}
