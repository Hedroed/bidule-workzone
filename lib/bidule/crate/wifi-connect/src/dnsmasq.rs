use std::process::{Child, Command};
use network_manager::Device;

use crate::errors::*;
use crate::config::Config;

pub fn start_dnsmasq(config: &Config, device: &Device) -> Result<Child> {
    let args = [
        &format!("--address=/#/{}", config.gateway), // Captive portail, any dns query return this ip address
        &format!("--dhcp-range={}", config.dhcp_range),
        &format!("--dhcp-option=option:router,{}", config.gateway),
        &format!("--interface={}", device.interface()),
        "--keep-in-foreground",
        "--bind-interfaces",
        "--except-interface=lo",
        "--conf-file",
        "--no-hosts",
    ];

    Command::new("dnsmasq")
        .args(&args)
        .spawn()
        .map_err(InternalError::Dnsmasq)
}

pub fn stop_dnsmasq(dnsmasq: &mut Child) -> Result<()> {
    info!("Stopping dnsmasq");
    dnsmasq.kill()?;

    Ok(())
}
