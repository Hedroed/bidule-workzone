#[macro_use]
extern crate tracing;

mod errors;
mod config;
mod network;
mod server;
mod dnsmasq;
mod logger;
mod exit;
mod privileges;

use tokio;
use log::instrument::Instrument;

use crate::errors::*;
use crate::config::get_config;
use crate::network::{init_networking, process_network_commands};
use crate::privileges::require_root;

#[tokio::main]
async fn main() -> std::result::Result<(), InternalError> {
    // _uninstall is a Guard that uninstall opentelemetry_jaeger tracer when dropped
    let _uninstall = logger::init();

    let span = trace_span!("root", "type" = "root");
    // _enter is a Guard
    let _enter = span.enter();

    debug!("Program started");

    let config = get_config();
    
    require_root().unwrap();
    
    init_networking(&config).unwrap();
    
    process_network_commands(&config)
    .in_current_span()
    .await?;
    
    debug!("Program done");
    Ok(())
}

