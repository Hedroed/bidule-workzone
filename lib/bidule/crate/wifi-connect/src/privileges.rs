use crate::errors::{Result, InternalError};

pub fn getuid() -> u32 {
    unsafe { libc::getuid() }
}

#[instrument]
pub fn require_root() -> Result<()> {
    if getuid() != 0 {
        Err(InternalError::RootPrivilegesRequired{ app: env!("CARGO_PKG_NAME").into() })
    } else {
        Ok(())
    }
}
