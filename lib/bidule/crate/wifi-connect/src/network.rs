use std::process;
use std::net::Ipv4Addr;
use std::collections::HashSet;
use std::time::Duration;

use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio;
use serde::{Serialize, Deserialize};
use log::instrument::Instrument;
use network_manager::{AccessPoint, AccessPointCredentials, Connection, ConnectionState,
                      Connectivity, Device, DeviceState, DeviceType, NetworkManager, Security,
                      ServiceState};
use crate::errors::*;
use crate::exit::trap_exit_signals;
use crate::config::Config;
use crate::dnsmasq::{start_dnsmasq, stop_dnsmasq};
use crate::server::start_server;

#[derive(Debug)]
pub enum NetworkCommand {
    Activate,
    // Timeout,
    Exit,
    Connect(NetworkConnect),
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct NetworkConnect {
    ssid: String,
    identity: String,
    passphrase: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Network {
    ssid: String,
    security: String,
}

pub enum NetworkCommandResponse {
    Networks(Vec<Network>),
}

struct NetworkCommandHandler {
    manager: NetworkManager,
    device: Device,
    access_points: Vec<AccessPoint>,
    portal_connection: Option<Connection>,
    config: Config,
    dnsmasq: process::Child,
    server_tx: Sender<NetworkCommandResponse>,
    network_rx: Receiver<NetworkCommand>,
    activated: bool,
}

impl std::fmt::Debug for NetworkCommandHandler {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NetworkCommandHandler")
         .field("device", &self.device)
         .field("access_points", &self.access_points)
         .field("portal_connection", &self.portal_connection)
         .field("config", &self.config)
         .field("dnsmasq", &self.dnsmasq)
         .field("server_tx", &self.server_tx)
         .field("network_rx", &self.network_rx)
         .field("activated", &self.activated)
         .finish()
    }
}

impl NetworkCommandHandler {
    async fn new(config: &Config) -> Result<Self> {
        let (network_tx, network_rx) = channel(32);

        Self::spawn_trap_exit_signals(network_tx.clone());

        let manager = NetworkManager::new();
        debug!("NetworkManager connection initialized");

        let device = find_device(&manager, &config.interface)?;

        let access_points = get_access_points(&device).in_current_span().await?;

        let portal_connection = Some(create_portal(&device, config)?);

        let dnsmasq = start_dnsmasq(config, &device)?;

        let (server_tx, server_rx) = channel(32);

        Self::spawn_server(config, server_rx, network_tx).await?;

        let config = config.clone();
        let activated = false;

        Ok(NetworkCommandHandler {
            manager,
            device,
            access_points,
            portal_connection,
            config,
            dnsmasq,
            server_tx,
            network_rx,
            activated,
        })
    }

    async fn spawn_server(
        config: &Config,
        // exit_tx: &Sender<Result<()>>,
        server_rx: Receiver<NetworkCommandResponse>,
        network_tx: Sender<NetworkCommand>,
    ) -> Result<()> {
        let gateway = config.gateway;
        let listening_port = config.listening_port;
        // let exit_tx_server = exit_tx.clone();
        let ui_directory = std::fs::canonicalize(config.ui_directory.join("build"))?;

        tokio::spawn(
            start_server(
                gateway,
                listening_port,
                server_rx,
                network_tx,
                // exit_tx_server,
                ui_directory,
            ).in_current_span()
        );
        Ok(())
    }

    fn spawn_trap_exit_signals(network_tx: Sender<NetworkCommand>) {
        let mut network_tx_clone = network_tx.clone();
        
        tokio::spawn(async move {
            trap_exit_signals().await.unwrap();
            network_tx_clone.send(NetworkCommand::Exit).await.unwrap();
        });
    }

    async fn run(&mut self) -> Result<()> {
        let activity_timeout = self.config.activity_timeout;

        let run_future = self.run_loop();

        let res: Result<()>;
        if activity_timeout == 0 {
            res = run_future.await
        } else {
            res = tokio::time::timeout(Duration::from_secs(activity_timeout), run_future).await.unwrap()
        }
        self.stop().await;

        res
    }

    async fn run_loop(&mut self) -> Result<()> {
        loop {
            let command = self.receive_network_command().await?;

            match command {
                NetworkCommand::Activate => {
                    self.activate().await?;
                },
                // NetworkCommand::Timeout => {
                //     if !self.activated {
                //         info!("Timeout reached. Exiting...");
                //         return Ok(());
                //     }
                // },
                NetworkCommand::Exit => {
                    info!("Exiting...");
                    return Ok(());
                },
                NetworkCommand::Connect(network) => {
                    if self.connect(&network.ssid, &network.identity, &network.passphrase).await? {
                        return Ok(());
                    }
                },
            }
        }
    }

    async fn receive_network_command(&mut self) -> Result<NetworkCommand> {
        match self.network_rx.recv().await {
            Some(command) => Ok(command),
            None => {
                Err(InternalError::RecvNetworkCommand())
            },
        }
    }

    async fn stop(&mut self) {
        let _ = stop_dnsmasq(&mut self.dnsmasq);

        if let Some(ref connection) = self.portal_connection {
            let _ = stop_portal(connection, &self.config).await;
        }
    }

    #[instrument]
    async fn activate(&mut self) -> Result<()> {
        self.activated = true;

        let networks = get_networks(&self.access_points);

        self.server_tx
            .send(NetworkCommandResponse::Networks(networks)).await
            .map_err(|_| InternalError::SendAccessPointSSIDs())
    }

    #[instrument]
    async fn connect(&mut self, ssid: &str, identity: &str, passphrase: &str) -> Result<bool> {
        delete_existing_connections_to_same_network(&self.manager, ssid);

        if let Some(ref connection) = self.portal_connection {
            stop_portal(connection, &self.config).await?;
        }

        self.portal_connection = None;

        self.access_points = get_access_points(&self.device).in_current_span().await?;

        if let Some(access_point) = find_first_access_point(&self.access_points, ssid) {
            let wifi_device = self.device.as_wifi_device().unwrap();

            info!("Connecting to access point '{}'...", ssid);

            let credentials = init_access_point_credentials(access_point, identity, passphrase);

            match wifi_device.connect(access_point, &credentials) {
                Ok((connection, state)) => {
                    if state == ConnectionState::Activated {
                        match wait_for_connectivity(&self.manager, 20).in_current_span().await {
                            Ok(has_connectivity) => {
                                if has_connectivity {
                                    info!("Internet connectivity established");
                                } else {
                                    warn!("Cannot establish Internet connectivity");
                                }
                            },
                            Err(err) => error!("Getting Internet connectivity failed: {}", err),
                        }

                        return Ok(true);
                    }

                    if let Err(err) = connection.delete() {
                        error!("Deleting connection object failed: {}", err)
                    }

                    warn!(
                        "Connection to access point not activated '{}': {:?}",
                        ssid, state
                    );
                },
                Err(e) => {
                    warn!("Error connecting to access point '{}': {}", ssid, e);
                },
            }

            self.access_points = get_access_points(&self.device).in_current_span().await?;
        }

        self.portal_connection = Some(create_portal(&self.device, &self.config)?);

        Ok(false)
    }
}

fn init_access_point_credentials(
    access_point: &AccessPoint,
    identity: &str,
    passphrase: &str,
) -> AccessPointCredentials {
    if access_point.security.contains(Security::ENTERPRISE) {
        AccessPointCredentials::Enterprise {
            identity: identity.to_string(),
            passphrase: passphrase.to_string(),
        }
    } else if access_point.security.contains(Security::WPA2)
        || access_point.security.contains(Security::WPA)
    {
        AccessPointCredentials::Wpa {
            passphrase: passphrase.to_string(),
        }
    } else if access_point.security.contains(Security::WEP) {
        AccessPointCredentials::Wep {
            passphrase: passphrase.to_string(),
        }
    } else {
        AccessPointCredentials::None
    }
}

pub async fn process_network_commands(config: &Config) -> Result<()> {
    let mut command_handler = NetworkCommandHandler::new(config).await?;
    command_handler.run().await?;
    Ok(())
}

pub fn init_networking(config: &Config) -> Result<()> {
    start_network_manager_service()?;

    delete_exising_wifi_connect_ap_profile(&config.ssid)
}

// return device from interface name or return first wifi device found
pub fn find_device(manager: &NetworkManager, interface: &Option<String>) -> Result<Device> {
    if let Some(ref interface) = *interface {
        let device = manager
            .get_device_by_interface(interface)
            .map_err(|e| InternalError::DeviceByInterface{interface: interface.clone(), source: e})?;

        info!("Targeted WiFi device: {}", interface);

        if *device.device_type() != DeviceType::WiFi {
            return Err(InternalError::NotAWiFiDevice(interface.clone()))
        }

        if device.get_state()? == DeviceState::Unmanaged {
            return Err(InternalError::UnmanagedDevice(interface.clone()))
        }

        Ok(device)
    } else {
        let devices = manager.get_devices()?;

        if let Some(device) = find_wifi_managed_device(devices)? {
            info!("WiFi device: {}", device.interface());
            Ok(device)
        } else {
            return Err(InternalError::NoWiFiDevice())
        }
    }
}

fn find_wifi_managed_device(devices: Vec<Device>) -> Result<Option<Device>> {
    for device in devices {
        if *device.device_type() == DeviceType::WiFi
            && device.get_state()? != DeviceState::Unmanaged
        {
            return Ok(Some(device));
        }
    }

    Ok(None)
}

#[instrument]
async fn get_access_points(device: &Device) -> Result<Vec<AccessPoint>> {
    let retries_allowed = 10;
    let mut retries = 0;

    // After stopping the hotspot we may have to wait a bit for the list
    // of access points to become available
    while retries < retries_allowed {
        let wifi_device = device.as_wifi_device().unwrap();
        let mut access_points = wifi_device.get_access_points()?;

        access_points.retain(|ap| ap.ssid().as_str().is_ok());

        // Purge access points with duplicate SSIDs
        let mut inserted = HashSet::new();
        access_points.retain(|ap| inserted.insert(ap.ssid.clone()));

        // Remove access points without SSID (hidden)
        access_points.retain(|ap| !ap.ssid().as_str().unwrap().is_empty());

        if !access_points.is_empty() {
            info!(
                "Access points: {:?}",
                get_access_points_ssids(&access_points)
            );
            return Ok(access_points);
        }

        retries += 1;
        debug!("No access points found - retry #{}", retries);
        tokio::time::delay_for(Duration::from_secs(1)).await;
    }

    warn!("No access points found - giving up...");
    Err(InternalError::NoAccessPoints())
}

fn get_access_points_ssids(access_points: &[AccessPoint]) -> Vec<&str> {
    access_points
        .iter()
        .map(|ap| ap.ssid().as_str().unwrap())
        .collect()
}

fn get_networks(access_points: &[AccessPoint]) -> Vec<Network> {
    access_points
        .iter()
        .map(|ap| get_network_info(ap))
        .collect()
}

fn get_network_info(access_point: &AccessPoint) -> Network {
    Network {
        ssid: access_point.ssid().as_str().unwrap().to_string(),
        security: get_network_security(access_point).to_string(),
    }
}

fn get_network_security(access_point: &AccessPoint) -> &str {
    if access_point.security.contains(Security::ENTERPRISE) {
        "enterprise"
    } else if access_point.security.contains(Security::WPA2)
        || access_point.security.contains(Security::WPA)
    {
        "wpa"
    } else if access_point.security.contains(Security::WEP) {
        "wep"
    } else {
        "none"
    }
}

fn find_first_access_point<'a>(access_points: &'a [AccessPoint], ssid: &str) -> Option<&'a AccessPoint> {
    for access_point in access_points.iter() {
        if let Ok(access_point_ssid) = access_point.ssid().as_str() {
            if access_point_ssid == ssid {
                return Some(access_point);
            }
        }
    }

    None
}

fn create_portal(device: &Device, config: &Config) -> Result<Connection> {
    let portal_passphrase = config.passphrase.as_ref().map(|p| p as &str);

    create_portal_impl(device, &config.ssid, &config.gateway, &portal_passphrase)
}

fn create_portal_impl(
    device: &Device,
    ssid: &str,
    gateway: &Ipv4Addr,
    passphrase: &Option<&str>,
) -> Result<Connection> {
    info!("Starting access point...");
    let wifi_device = device.as_wifi_device().unwrap();
    let (portal_connection, _) = wifi_device.create_hotspot(ssid, *passphrase, Some(*gateway))?;
    info!("Access point '{}' created", ssid);
    Ok(portal_connection)
}

async fn stop_portal(connection: &Connection, config: &Config) -> Result<()> {
    info!("Stopping access point '{}'...", config.ssid);
    connection.deactivate()?;
    connection.delete()?;
    tokio::time::delay_for(Duration::from_secs(1)).await;
    info!("Access point '{}' stopped", config.ssid);
    Ok(())
}

async fn wait_for_connectivity(manager: &NetworkManager, timeout: u64) -> Result<bool> {
    let mut total_time = 0;

    loop {
        let connectivity = manager.get_connectivity()?;

        if connectivity == Connectivity::Full || connectivity == Connectivity::Limited {
            debug!(
                "Connectivity established: {:?} / {}s elapsed",
                connectivity, total_time
            );

            return Ok(true);
        } else if total_time >= timeout {
            debug!(
                "Timeout reached in waiting for connectivity: {:?} / {}s elapsed",
                connectivity, total_time
            );

            return Ok(false);
        }

        tokio::time::delay_for(Duration::from_secs(1)).await;

        total_time += 1;

        debug!(
            "Still waiting for connectivity: {:?} / {}s elapsed",
            connectivity, total_time
        );
    }
}

pub fn start_network_manager_service() -> Result<()> {
    let state = match NetworkManager::get_service_state() {
        Ok(state) => state,
        _ => {
            info!("Cannot get the NetworkManager service state");
            return Ok(());
        },
    };

    if state != ServiceState::Active {
        let state = NetworkManager::start_service(15)?;
        if state != ServiceState::Active {
            return Err(InternalError::StartActiveNetworkManager());
        } else {
            info!("NetworkManager service started successfully");
        }
    } else {
        debug!("NetworkManager service already running");
    }

    Ok(())
}

fn delete_exising_wifi_connect_ap_profile(ssid: &str) -> Result<()> {
    let manager = NetworkManager::new();

    for connection in &manager.get_connections()? {
        if is_access_point_connection(connection) && is_same_ssid(connection, ssid) {
            info!(
                "Deleting already created by WiFi Connect access point connection profile: {:?}",
                connection.settings().ssid,
            );
            connection.delete()?;
        }
    }

    Ok(())
}

fn delete_existing_connections_to_same_network(manager: &NetworkManager, ssid: &str) {
    let connections = match manager.get_connections() {
        Ok(connections) => connections,
        Err(e) => {
            error!("Getting existing connections failed: {}", e);
            return;
        },
    };

    for connection in &connections {
        if is_wifi_connection(connection) && is_same_ssid(connection, ssid) {
            info!(
                "Deleting existing WiFi connection to the same network: {:?}",
                connection.settings().ssid,
            );

            if let Err(e) = connection.delete() {
                error!("Deleting existing WiFi connection failed: {}", e);
            }
        }
    }
}

fn is_same_ssid(connection: &Connection, ssid: &str) -> bool {
    connection_ssid_as_str(connection) == Some(ssid)
}

fn connection_ssid_as_str(connection: &Connection) -> Option<&str> {
    // An access point SSID could be random bytes and not a UTF-8 encoded string
    connection.settings().ssid.as_str().ok()
}

fn is_access_point_connection(connection: &Connection) -> bool {
    is_wifi_connection(connection) && connection.settings().mode == "ap"
}

fn is_wifi_connection(connection: &Connection) -> bool {
    connection.settings().kind == "802-11-wireless"
}
