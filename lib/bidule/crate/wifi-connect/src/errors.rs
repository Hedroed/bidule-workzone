use thiserror::Error;

#[derive(Error, Debug)]
pub enum InternalError {
    #[error("Sending access point SSIDs failed")]
    SendAccessPointSSIDs(),

    #[error("Receiving network command failed")]
    RecvNetworkCommand(),

    #[error("Cannot find network device with interface name '{interface:?}'")]
    DeviceByInterface {
        interface: String,
        source: network_manager::errors::Error,
    },

    #[error("Not a WiFi device: {0:?}")]
    NotAWiFiDevice(String),

    #[error("Unmanaged device: {0:?}")]
    UnmanagedDevice(String),

    #[error("Cannot find a WiFi device")]
    NoWiFiDevice(),

    #[error("Getting access points failed")]
    NoAccessPoints(),

    #[error("Starting the NetworkManager service with active state failed")]
    StartActiveNetworkManager(),

    #[error("You need root privileges to run {app:?}")]
    RootPrivilegesRequired {
        app: String,
    },

    #[error("Spawning dnsmasq failed")]
    Dnsmasq(#[from] std::io::Error),
    
    #[error(transparent)]
    RecvError(#[from] std::sync::mpsc::RecvError),
    
    #[error(transparent)]
    NetworkManagerError(#[from] network_manager::errors::Error),
}

pub type Result<T> = std::result::Result<T, InternalError>;

// pub fn exit_code(e: &Error) -> i32 {
//     match *e.kind() {
//         ErrorKind::Dnsmasq => 3,
//         ErrorKind::RecvAccessPointSSIDs => 4,
//         ErrorKind::SendAccessPointSSIDs => 5,
//         ErrorKind::SerializeAccessPointSSIDs => 6,
//         ErrorKind::RecvNetworkCommand => 7,
//         ErrorKind::SendNetworkCommandActivate => 8,
//         ErrorKind::SendNetworkCommandConnect => 9,
//         ErrorKind::DeviceByInterface(_) => 10,
//         ErrorKind::NotAWiFiDevice(_) => 11,
//         ErrorKind::NoWiFiDevice => 12,
//         ErrorKind::NoAccessPoints => 13,
//         ErrorKind::CreateCaptivePortal => 14,
//         ErrorKind::StopAccessPoint => 15,
//         ErrorKind::DeleteAccessPoint => 16,
//         ErrorKind::StartHTTPServer(_, _) => 17,
//         ErrorKind::StartActiveNetworkManager => 18,
//         ErrorKind::StartNetworkManager => 19,
//         ErrorKind::BlockExitSignals => 21,
//         ErrorKind::TrapExitSignals => 22,
//         ErrorKind::RootPrivilegesRequired(_) => 23,
//         ErrorKind::UnmanagedDevice(_) => 24,
//         _ => 1,
//     }
// }
