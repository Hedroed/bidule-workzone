// use tokio::sync::mpsc::Sender;

use tokio::signal;

use crate::errors::*;

// pub fn exit(exit_tx: &Sender<Result<()>>, error: InternalError) {
//     let _ = exit_tx.send(Err(error));
// }

/// Block exit signals from the main thread with mask inherited by children
// pub fn block_exit_signals() -> Result<()> {
//     let mask = create_exit_sigmask();
//     mask.thread_block()
//         .map_err(|_| InternalError::BlockExitSignals())
// }

/// Trap exit signals from a signal handling thread
pub async fn trap_exit_signals() -> Result<()> {
    signal::ctrl_c().await?;

    info!("Received ctrl_c");
    Ok(())
}

// fn create_exit_sigmask() -> SigSet {
//     let mut mask = SigSet::empty();

//     mask.add(SIGINT);
//     mask.add(SIGQUIT);
//     mask.add(SIGTERM);
//     mask.add(SIGHUP);

//     mask
// }
