from PIL import Image
import sys

def encode(img_name, font_name, width, height):

    assert width <= 8, "width must be 8 or less because we encode rows on 1 byte"

    img_rgb = Image.open(img_name)
    img = img_rgb.convert('1')

    data = img.getdata()

    ret = b''

    for i in range(128):

        for y in range(height):

            pos_x = i * width + y * img.width

            d = [ data[i] for i in range(pos_x,pos_x+width)]
            # print(d)
            # input()

            bi = ''.join('1' if j == 0 else '0' for j in d)  # Convert list int to bin string (use this method for safety, 0 => '0' other mean '1')
            bi = bi.ljust(8, '0')  ## add missing 0 because font width are 8 or less
            bi = bi[::-1]  ## reverse endianness for decoding optimisation

            b = int(bi, 2)

            ret += bytes([b])


    with open('font_%s.bin' % font_name, 'wb') as f:
        f.write(ret)

    return len(ret)



l = encode("fontmap_normal.png", "default_normal", 5, 10)
print('default_normal: %d bytes written' % l)

l = encode("fontmap_bold.png", "default_bold", 6, 11)
print('default_bold: %d bytes written' % l)
