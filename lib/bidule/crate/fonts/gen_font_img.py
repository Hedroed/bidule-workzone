from PIL import ImageFont, ImageDraw, Image
import sys

asc = bytes(range(128))
text = asc.decode('ascii', 'ignore')
print(text.encode())


def encode(font, letter_width):
    _, height = font.getsize(text)
    true_width = 128 * letter_width

    image = Image.new("1", (true_width, height), 255)
    draw = ImageDraw.Draw(image)

    for i in range(128):
        draw.text((i*letter_width-1, 0), text[i], font=font, fill=0)

    return image



font = ImageFont.truetype('/usr/share/fonts/TTF/DejaVuSansMono.ttf', 10)
img = encode(font, 5)
img.save('test.png')

font = ImageFont.truetype('/usr/share/fonts/TTF/DejaVuSansMono-Bold.ttf', 11)
img = encode(font, 6)
img.save('test_bold.png')
