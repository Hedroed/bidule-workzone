use pngencode::{BitDepth, ColorType, EncoderBuilder};

use std::error::Error;
// For reading and opening files
use std::env;
use std::path::Path;
use std::fs::{File, read};
use std::io::BufWriter;

fn main() -> Result<(), Box<dyn Error>> {

    let args: Vec<String> = env::args().collect();
    let in_filename = &args[1];

    let path = Path::new(r"./image.png");
    let file = File::create(path)?;
    let ref mut w = BufWriter::new(file);

    let buf: Vec<u8> = read(in_filename)?;

    let mut builder = EncoderBuilder::new(122, 250); // Width is 12 pixels and height is 250.
    builder.set_color(ColorType::Grayscale);
    builder.set_depth(BitDepth::One);
    let mut encoder = builder.build(w)?;
    
    encoder.write(&buf)?;
    let (_output, result) = encoder.finish();
    result?;
    Ok(())
}


