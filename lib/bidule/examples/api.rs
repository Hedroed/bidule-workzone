use hyper_tls::HttpsConnector;
use hyper::{Client, Body, Method, Request, Uri};
use hyper::body::HttpBody as _;
use tokio::io::{stdout, AsyncWriteExt as _};

use base64::encode;


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>>{
    
    let client_id = "4cf75db2b49246faa9a83a0e48a8bc56";
    let client_secret = "f15aef38d42344df83d1c68eeb7774ff";

    let token = encode(format!("{}:{}", client_id, client_secret));
    
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);

    let req = Request::builder()
        .method(Method::POST)
        .uri("https://accounts.spotify.com/api/token")
        .header("content-type", "application/x-www-form-urlencoded")
        .header("authorization", format!("Basic {}", token))
        .body(Body::from("grant_type=client_credentials"))?;

    let mut resp = client.request(req).await?;

    println!("Response: {}", resp.status());
    while let Some(chunk) = resp.body_mut().data().await {
        stdout().write_all(&chunk?).await?;
    }

    let req = Request::builder()
        .method(Method::GET)
        .uri("https://api.spotify.com/v1/me/player/devices")
        .header("content-type", "application/json")
        .header("authorization", "Bearer BQBNUJ8Lf1VXr5RgKgRoPDAGEWPZ_QCkOS4tK_ovQUr994nQpwIlpmOiwvjn5IWLQIqL8JfKqOMTxckdwp1R7Am2OreCBk4r3i9Mgzo1vSoJMi7NGtp9WSihlqZio7l4Jl3gnOUzSRQqEGXrisrZ1b1ecvfSww0fb0gpuOtt179YuLL0QP5gYRIds_vwg6vWPCzVG9aNj4LC1RfWRXVeJO5hJmp4ez1Rx0kCuDiZOy6UfzCDQxTaiQkjX_KNsSKryVZBPczGcWQlYV_52u8o8Q")
        .body(Body::from(""))?;

    let mut resp = client.request(req).await?;

    println!("Response: {}", resp.status());
    while let Some(chunk) = resp.body_mut().data().await {
        stdout().write_all(&chunk?).await?;
    }

    Ok(())
}