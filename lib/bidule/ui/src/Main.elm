module Main exposing (..)

import Browser
import Html exposing (Html, a, button, div, img, input, text)
import Html.Attributes exposing (class, href, src, type_)
import Html.Events exposing (on, onCheck, onClick)
import Http
import Json.Decode
import Time exposing (Posix)



---- MODEL ----


type alias Model =
    { count : Int
    , stopped : Bool
    , viewError : Bool
    }


initModel : Model
initModel =
    { count = 0
    , stopped = False
    , viewError = False
    }


init : ( Model, Cmd Msg )
init =
    ( initModel, Cmd.none )



---- UPDATE ----


type Msg
    = NoOp
    | ButtonUp
    | ButtonDown
    | ButtonEnter
    | ButtonReturn
    | ToggleSwitch2 Bool
    | ToggleStop Bool
    | ViewError
    | Tick Posix
    | HttpResponse (Result Http.Error String)


buttonAction : String -> Cmd Msg
buttonAction action =
    Http.get
        { url = "/api/" ++ action
        , expect = Http.expectString HttpResponse
        }


switchAction : String -> Bool -> Cmd Msg
switchAction action status =
    Http.get
        { url =
            "/api/" ++ action
                ++ (if status then
                        "/right"

                    else
                        "/left"
                   )
        , expect = Http.expectString HttpResponse
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ButtonUp ->
            ( model, buttonAction "up" )

        ButtonDown ->
            ( model, buttonAction "down" )

        ButtonEnter ->
            ( model, buttonAction "enter" )

        ButtonReturn ->
            ( model, buttonAction "return" )

        ToggleSwitch2 status ->
            ( model, switchAction "switch2" status )

        ToggleStop status ->
            ( { model | stopped = status }, Cmd.none )

        Tick _ ->
            if model.stopped then
                ( model, Cmd.none )

            else
                ( { model | count = model.count + 1 }, Cmd.none )

        HttpResponse _ ->
            ( model, Cmd.none )

        ViewError ->
            ( { model | viewError = True }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ viewScreen model
        , div []
            [ button [ onClick ButtonReturn ] [ text "Return" ]
            , button [ onClick ButtonUp ] [ text "Up" ]
            , button [ onClick ButtonDown ] [ text "Down" ]
            , button [ onClick ButtonEnter ] [ text "Enter" ]
            , input [ onCheck ToggleSwitch2, type_ "checkbox" ] []
            , input [ onCheck ToggleStop, type_ "checkbox" ] []
            ]
        , div []
            [  a [ href "/oauth" ] [ text "Authorize Spotify" ]
            ]
        ]


viewScreen : Model -> Html Msg
viewScreen { count, viewError } =
    if viewError then
        div [ class "display" ]
            [ text "Offline" ]

    else
        img [ class "display", src ("/view#" ++ String.fromInt count), on "error" (Json.Decode.succeed ViewError) ] []



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Time.every 2000 Tick
