use invert_rect::InvertRect;
use pencil::*;
use std::string::String;

// draw a list of 6 items
pub fn view_list(canvas: &mut Canvas, items: &[String], depth: usize, cursor: usize) {
    static OFFSET_TOP: usize = 12;
    const WIDTH: usize = 113; // size of 16 chars
    const HEIGHT: usize = 14;
    const FULL_HEIGHT: usize = 99;

    let line = components::line::Line::new(
        WIDTH,
        components::line::Direction::Vertical,
        OFFSET_TOP,
        FULL_HEIGHT,
    );
    canvas.display_list.add(Box::new(line));

    if depth > 1 {
        let text = components::word::Word::new(1, OFFSET_TOP + 2, format!("..({})", depth), true);
        canvas.display_list.add(Box::new(text));
    } else {
        let text = components::word::Word::new(1, OFFSET_TOP + 2, ".".to_string(), true);
        canvas.display_list.add(Box::new(text));
    }

    let line = components::line::Line::new(
        OFFSET_TOP + HEIGHT,
        components::line::Direction::Horizontal,
        0,
        WIDTH,
    );
    canvas.display_list.add(Box::new(line));
    let mut x = HEIGHT;

    for idx in 0..6 {
        let line = components::line::Line::new(
            OFFSET_TOP + x + HEIGHT,
            components::line::Direction::Horizontal,
            0,
            WIDTH,
        );
        canvas.display_list.add(Box::new(line));
        if let Some(item) = items.get(idx) {
            let text = components::word::Word::new(1, OFFSET_TOP + x + 2, item.to_string(), true);
            canvas.display_list.add(Box::new(text));
        }
        if cursor == idx {
            let rect = InvertRect::new(0, OFFSET_TOP + x, WIDTH, HEIGHT);
            canvas.display_list.add(Box::new(rect));
        }
        x += HEIGHT;
    }
}
