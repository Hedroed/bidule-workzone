use log::info;

use crate::app::ViewRoute;

#[derive(Debug)]
pub struct ScriptsState {
    pub level: usize,
    pub items: Vec<String>,
    pub cursor: usize,
    pub shutdown: Option<bool>,
}

pub async fn init() -> ViewRoute {
    ViewRoute::Scripts(ScriptsState {
        level: 0,
        items: vec![
            "123456789ABCDEFG".to_string(),
            "internet_test/".to_string(),
            "challs_tests/".to_string(),
            "tools/".to_string(),
            "Set_static_ip".to_string(),
            "Shutdown".to_string(),
            "Reboot".to_string(),
        ],
        cursor: 0,
        shutdown: None,
    })
}
