use log::info;
use tokio::process::Command;

use crate::events::BiduleEvent;

use super::model::ScriptsState;

pub async fn update(state: &mut ScriptsState, event: &BiduleEvent) {
    if let Some(_) = &state.shutdown {
        update_shutdown(state, event).await;
    } else {
        match event {
            BiduleEvent::ButtonUp => {
                if state.cursor > 0 {
                    state.cursor -= 1;
                }
            },
            BiduleEvent::ButtonDown => {
                
                if state.cursor < 5 {
                    state.cursor += 1;
                };
            }
            BiduleEvent::ButtonEnter => {
                info!("Click Enter");
                if let Some(selected) = state.items.get(state.cursor) {
                    if selected == "Shutdown" {
                        state.shutdown = Some(false);
                        // power_off().await.expect("Cannot poweroff");
                    }
                };
            }
            BiduleEvent::ButtonReturn => {
                info!("Click Return");
            }
            _ => {}
        }
    }
    
}

pub async fn update_shutdown(state: &mut ScriptsState, event: &BiduleEvent) {
    match event {
        BiduleEvent::ButtonUp => {
            state.shutdown = Some(false);
        },
        BiduleEvent::ButtonDown => {
            state.shutdown = Some(true);
        }
        BiduleEvent::ButtonEnter => {
            if let Some(s) = state.shutdown {
                if s {
                    power_off().await.expect("Unable to poweroff");
                } else {
                    state.shutdown = None;
                }
            }
        }
        BiduleEvent::ButtonReturn => {
            info!("exit shutdown");
            state.shutdown = None;
        }
        _ => {}
    };
}

async fn power_off() -> Result<(), Box<dyn std::error::Error>> {
    info!("Stopping the system with poweroff");
    let output = Command::new("poweroff").output();

    let output = output.await?;

    assert!(output.status.success());
    Ok(())
}
