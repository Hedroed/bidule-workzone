use pencil::*;
use std::string::String;
use log::{debug, info};

use super::model::ScriptsState;
use crate::view_utils::view_list;

pub fn view(state: &ScriptsState) -> Canvas {
    debug!("Drawing view");

    // create a canvas to draw on
    let mut canvas = Canvas::new(250, 122);

    let line = components::line::FullLine::new(12, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));

    let text = components::word::Word::new(1, 0, String::from("ip:192.168.234.234"), false);
    canvas.display_list.add(Box::new(text));

    let text = components::word::Word::new(133, 0, String::from("dns:192.168.234.234"), false);
    canvas.display_list.add(Box::new(text));

    if let Some(shutdown_state) = &state.shutdown {
        let text = components::word::Word::new(30, 60, String::from(format!("SHUTDOWNING: {:?}", shutdown_state)), false);
        canvas.display_list.add(Box::new(text));
    } else {
        view_list(&mut canvas, &state.items[..], state.level + 1, state.cursor);
    }

    static BOTTOM_OFFSET: usize = 110;

    let line = components::line::FullLine::new(BOTTOM_OFFSET, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));

    let icon = components::btn_icon::BtnIcon::new(15, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(70, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(125, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(180, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));

    let text = components::word::Word::new(30, BOTTOM_OFFSET + 2, String::from("Return"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(85, BOTTOM_OFFSET + 2, String::from("Up"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(140, BOTTOM_OFFSET + 2, String::from("Down"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(195, BOTTOM_OFFSET + 2, String::from("Enter"), false);
    canvas.display_list.add(Box::new(text));

    canvas
}