use hyper::body::Body;
use pngencode::{BitDepth, ColorType, EncoderBuilder};
use rppal::gpio::Level;
use std::{collections::HashMap, path::PathBuf};
use std::{str::FromStr};
use std::{convert::Infallible, sync::Arc};
use tokio::sync::{mpsc::Sender, RwLock};
use log::{debug, error, info};
use warp::{Filter, Reply, http::Uri, reply::Response};


use crate::{controls::get_event, events::BiduleEvent};

pub async fn serve(event_emitter: Sender<BiduleEvent>, view_buffer: Arc<RwLock<Vec<u8>>>) {
    let ui_directory: PathBuf = "./ui/build/".parse().unwrap();

    let index = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file(ui_directory.join("index.html")));
    let favicon = warp::get()
        .and(warp::path("favicon.ico"))
        .and(warp::fs::file(ui_directory.join("favicon.ico")));

    let static_dir = warp::path("static").and(warp::fs::dir(ui_directory.join("static")));

    let oauth_callback = warp::path!("callback")
        .and(warp::get())
        .and(warp::query())
        .and(with_emitter(event_emitter.clone()))
        .and_then(oauth_callback_handler);

    let oauth_request = warp::path!("oauth")
        .and(warp::get())
        .and_then(oauth_request_handler);

    let gpio_events = warp::path!("api" / "gpio" / u8)
        .and(warp::get())
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);

    let gpio_high_events = warp::path!("api" / "gpio" / u8 / "high")
        .and(warp::get())
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_high_handler);

    let up_events = warp::path!("api" / "up")
        .and(warp::get())
        .map(|| 20u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);
    let down_events = warp::path!("api" / "down")
        .and(warp::get())
        .map(|| 19u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);
    let back_events = warp::path!("api" / "return")
        .and(warp::get())
        .map(|| 21u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);
    let enter_events = warp::path!("api" / "enter")
        .and(warp::get())
        .map(|| 13u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);
    let switch2_low_events = warp::path!("api" / "switch2" / "left")
        .and(warp::get())
        .map(|| 6u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_low_handler);
    let switch2_high_events = warp::path!("api" / "switch2" / "right")
        .and(warp::get())
        .map(|| 6u8) // set pin = 8 in gpio_low_handler
        .and(with_emitter(event_emitter.clone()))
        .and_then(gpio_high_handler);

    let view = warp::get()
        .and(warp::path("view"))
        .and(with_view(view_buffer))
        .and_then(view_reply);

    let routes = index
        .or(favicon)
        .or(static_dir)
        .or(oauth_callback)
        .or(oauth_request)
        .or(gpio_events)
        .or(gpio_high_events)
        .or(up_events)
        .or(down_events)
        .or(back_events)
        .or(enter_events)
        .or(switch2_low_events)
        .or(switch2_high_events)
        .or(view);
    // Wrap all the routes with a filter that creates a `tracing` span for
    // each request we receive, including data about the request.
        // .with(warp::trace::request());

    warp::serve(routes).run(([0, 0, 0, 0], 3030)).await;
}

fn with_emitter(
    request_state: Sender<BiduleEvent>,
) -> impl Filter<Extract = (Sender<BiduleEvent>,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || request_state.clone())
}

fn with_view(
    buf: Arc<RwLock<Vec<u8>>>,
) -> impl Filter<Extract = (Arc<RwLock<Vec<u8>>>,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || buf.clone())
}

// #[derive(Debug, Serialize, Deserialize, PartialEq)]
// struct GpioEventJson {
//     pin: u8
// }

// fn json_body() -> impl Filter<Extract = (GpioEventJson,), Error = warp::Rejection> + Clone {
//     // When accepting a body, we want a JSON body
//     // (and to reject huge payloads)...
//     warp::body::content_length_limit(1024 * 16).and(warp::body::json())
// }

async fn view_reply(view_buffer: Arc<RwLock<Vec<u8>>>) -> Result<Response, Infallible> {
    // let raw_buf = render_raw(view()).unwrap();

    let buf = Vec::new();

    let mut builder = EncoderBuilder::new(250, 122); // Width is 122 pixels and height is 250.
    builder.set_color(ColorType::Grayscale);
    builder.set_depth(BitDepth::Eight);
    let mut encoder = builder.build(buf).unwrap();

    let raw_buf = view_buffer.read().await;

    encoder.write(raw_buf.as_ref()).unwrap();
    let (output, result) = encoder.finish();
    result.unwrap();

    let content_size = output.len();

    let mut resp = Response::new(Body::from(output));
    resp.headers_mut()
        .insert("Content-length", content_size.to_string().parse().unwrap());
    resp.headers_mut()
        .insert("Content-type", "image/png".parse().unwrap());

    Ok(resp)
}

async fn gpio_low_handler(
    pin: u8,
    event_emitter: Sender<BiduleEvent>,
) -> Result<Response, Infallible> {
    gpio_event_handler(pin, event_emitter, Level::Low).await
}

async fn gpio_high_handler(
    pin: u8,
    event_emitter: Sender<BiduleEvent>,
) -> Result<Response, Infallible> {
    gpio_event_handler(pin, event_emitter, Level::High).await
}

async fn gpio_event_handler(
    pin: u8,
    event_emitter: Sender<BiduleEvent>,
    level: Level,
) -> Result<Response, Infallible> {
    let event = get_event(pin, level);

    event_emitter
        .send(event)
        .await
        .map(|_| warp::reply().into_response())
        .or(Ok(warp::http::StatusCode::BAD_REQUEST.into_response()))
}

async fn oauth_callback_handler(
    args: HashMap<String, String>,
    event_emitter: Sender<BiduleEvent>,
) -> Result<impl Reply, Infallible> {
    if let Some(state) = args.get("state") {
        if let Some(code) = args.get("code") {
            debug!("Get code {} & state {}", code, state);

            event_emitter.send(BiduleEvent::OauthCallback(code.clone())).await.unwrap();

        } else if let Some(error) = args.get("error") {
            error!("Get error {} & state {}", error, state);
        } else {
            error!("Unknown error");
        }
    }

    Ok(warp::redirect(Uri::from_static("/")))
}

async fn oauth_request_handler() -> Result<impl Reply, Infallible> {
    let redirect_uri = crate::oauth::gen_authorization_uri();
    Ok(warp::redirect(Uri::from_str(&redirect_uri).unwrap()))
}
