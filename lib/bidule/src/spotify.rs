// use chrono::prelude::*;
// use serde::{Serialize, Deserialize};

//  built-in battery
use std::collections::HashMap;
use std::string::String;
use std::error::Error;

use crate::oauth::{CACHE_PATH, Token, token_from_cache};

use log::error;
use rspotify_model::{context::CurrentPlaybackContext, DevicePayload};
use serde_json::{Map, Value};

/// Spotify API object
#[derive(Debug, Clone)]
pub struct Spotify {
    pub prefix: String,
    token: Token,
    client: reqwest::Client,
    initialized: bool,
}

impl Spotify {

    pub async fn new() -> Spotify {
        match token_from_cache(CACHE_PATH.into()).await {
            Ok(token) => {
                Spotify {
                    prefix: "https://api.spotify.com/v1/".to_owned(),
                    token,
                    client: reqwest::Client::new(),
                    initialized: true,
                }
            },
            Err(err) => {
                error!("couldn't load {:?}: {:?}", CACHE_PATH, err.to_string());
                Spotify {
                    prefix: "https://api.spotify.com/v1/".to_owned(),
                    token: Token::new(CACHE_PATH.into()),
                    client: reqwest::Client::new(),
                    initialized: false,
                }
            }
        }
    }

    pub fn is_ready(&self) -> bool {
        self.initialized
    }

    pub async fn oauth_authorization_code(&mut self, code: &str) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.token.init(code).await?;
        self.initialized = true;
        Ok(())
    }

    async fn auth_headers(&mut self) -> Result<String, Box<dyn Error + Send + Sync>> {
        let token = self.token.get_access_token().await?;
        Ok("Bearer ".to_owned() + &token)
    }

    pub async fn current_playback(
        &mut self,
    ) -> Result<Option<CurrentPlaybackContext>, Box<dyn Error + Send + Sync>> {
        let mut params = HashMap::new();
        params.insert("market".to_owned(), "FR".to_owned());


        self.client
            .get("https://api.spotify.com/v1/me/player")
            .header("authorization", self.auth_headers().await?)
            .query(&params)
            .send()
            .await?
            .json::<Option<CurrentPlaybackContext>>()
            .await
            .map_err(|err| err.into())
    }

    ///[get a users available devices](https://developer.spotify.com/web-api/get-a-users-available-devices/)
    ///Get a User’s Available Devices
    pub async fn device(&mut self) -> Result<DevicePayload, Box<dyn Error + Send + Sync>> {
        self.client
            .get("https://api.spotify.com/v1/me/player/devices")
            .header("authorization", self.auth_headers().await?)
            .send()
            .await?
            .json::<DevicePayload>()
            .await
            .map_err(|err| err.into())
    }

    ///[transfer a users playback](https://developer.spotify.com/web-api/transfer-a-users-playback/)
    ///Transfer a User’s Playback
    ///Note: Although an array is accepted, only a single device_id is currently
    /// supported. Supplying more than one will return 400 Bad Request
    ///            Parameters:
    ///- device_id - transfer playback to this device
    ///- force_play - true: after transfer, play. false:
    ///keep current state.
    pub async fn transfer_playback(
        &mut self,
        device_id: &str,
        force_play: bool,
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        let device_ids = vec![device_id.to_owned()];
        let mut payload = Map::new();
        payload.insert("device_ids".to_owned(), device_ids.into());
        payload.insert("play".to_owned(), force_play.into());
        
        self.client
            .put("https://api.spotify.com/v1/me/player")
            .header("authorization", self.auth_headers().await?)
            .json(&Value::Object(payload))
            .send()
            .await?
            .bytes()
            .await
            .and(Ok(()))
            .map_err(|err| err.into())
    }
}
