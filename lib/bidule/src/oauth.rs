use chrono::prelude::*;
use log::info;
use tokio::{fs::{File, OpenOptions}, io::{AsyncReadExt, AsyncWriteExt}};
use std::{error::Error, path::PathBuf};

use base64::encode;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use serde_json;

pub static CLIENT_ID: &str = "4cf75db2b49246faa9a83a0e48a8bc56";
pub static CLIENT_SECRET: &str = "f15aef38d42344df83d1c68eeb7774ff";

pub static REDIRECT_URI: &str = "http://localhost:3030/callback";
// pub static REDIRECT_URI: &str = "http://192.168.1.17:3030/callback";

pub static CACHE_PATH: &str = ".spotify_token_cache.json";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OauthToken {
    pub access_token: String,
    pub refresh_token: String,
    pub expires_in: i64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Token {
    pub access_token: String,
    pub refresh_token: String,
    pub expires_at: i64,
    cache_path: PathBuf,
}

impl Token {

    pub fn new(cache_path: PathBuf) -> Token {
        Token {
            access_token: String::new(),
            refresh_token: String::new(),
            expires_at: 0,
            cache_path: cache_path,
        }
    }

    pub async fn init(&mut self, code: &str) -> Result<(), Box<dyn Error + Send + Sync>> {
        match get_token_requests(code).await {
            Ok(token_info) => {
                let now: DateTime<Utc> = Utc::now();
                
                self.access_token = token_info.access_token;
                self.refresh_token = token_info.refresh_token;
                self.expires_at = now.timestamp() + token_info.expires_in;

                self.save_token_info().await;
                Ok(())
            }
            Err(err) => Err(err)
        }
    }

    /// after refresh access_token, the response may be empty
    /// when refresh_token again
    pub async fn refresh_access_token(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        match refresh_token_requests(&self.refresh_token).await {
            Ok(token_info) => {
                self.access_token = token_info.access_token;
                self.refresh_token = token_info.refresh_token;

                let now: DateTime<Utc> = Utc::now();
                self.expires_at = now.timestamp() + token_info.expires_in;

                self.save_token_info().await;
                Ok(())
            }
            Err(err) => Err(err)
        }
    }

    pub async fn save_token_info(&self) {

        match serde_json::to_string(&self) {
            Ok(token_info_string) => {
                let path = self.cache_path.as_path();
                let mut file = OpenOptions::new()
                    .write(true)
                    .create(true)
                    .open(path)
                    .await
                    .unwrap_or_else(|_| panic!("create file {:?} error", path.display()));
                file.set_len(0).await.unwrap_or_else(|_| {
                    panic!(
                        "clear original spotify-token-cache file [{:?}] failed",
                        path.display()
                    )
                });
                file.write_all(token_info_string.as_bytes())
                    .await
                    .expect("error when write file");
            }
            Err(why) => {
                panic!(
                    "couldn't convert token_info to string: {} ",
                    why.to_string()
                );
            }
        }
    }

    fn is_token_expired(&self) -> bool {
        let now: DateTime<Utc> = Utc::now();
        // 10s as buffer time
        now.timestamp() > self.expires_at - 10
    }

    pub async fn get_access_token(&mut self) -> Result<String, Box<dyn Error + Send + Sync>> {
        if self.is_token_expired() {
            self.refresh_access_token().await?;
        }
        Ok(self.access_token.clone())
    }
}

fn gen_basic_auth() -> String {
    encode(format!("{}:{}", CLIENT_ID, CLIENT_SECRET))
}

fn gen_random_state() -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect()
}

pub fn gen_authorization_uri() -> String {
    // https://accounts.spotify.com/authorize?client_id=4cf75db2b49246faa9a83a0e48a8bc56&response_type=code&redirect_uri=http://localhost:3030/callback&state=AZERTYUIO
    let params = [
        ("client_id", CLIENT_ID),
        ("response_type", "code"),
        ("redirect_uri", REDIRECT_URI),
        ("state", &gen_random_state()),
        ("scope", "user-read-playback-state user-modify-playback-state"),
    ];

    let encoded_params = serde_urlencoded::to_string(params).unwrap();

    format!(
        "https://accounts.spotify.com/authorize?{}",
        encoded_params
    )
}

pub async fn token_from_cache(cache_path: PathBuf) -> Result<Token, Box<dyn Error + Send + Sync>> {
    let mut file = File::open(&cache_path).await?;
    let mut token_info_string = String::new();
    file.read_to_string(&mut token_info_string).await?;
    let mut token_info: Token = serde_json::from_str(&token_info_string)?;
    if token_info.is_token_expired() {
        info!("cached token is expired refreshing...");
        token_info.refresh_access_token().await?;
    }
    Ok(token_info)
}

async fn get_token_requests(code: &str) -> Result<OauthToken, Box<dyn Error + Send + Sync>> {
    let params = [
        ("grant_type", "authorization_code"),
        ("code", code),
        ("redirect_uri", REDIRECT_URI),
    ];
    let client = reqwest::Client::new();

    client
        .post("https://accounts.spotify.com/api/token")
        .header("content-type", "application/x-www-form-urlencoded")
        .header("authorization", format!("Basic {}", gen_basic_auth()))
        .form(&params)
        .send()
        .await?
        .json::<OauthToken>()
        .await
        .map_err(|err| err.into())

}

async fn refresh_token_requests(refresh_token: &str) -> Result<OauthToken, Box<dyn Error + Send + Sync>> {
    let params = [
        ("grant_type", "refresh_token"),
        ("refresh_token", refresh_token),
    ];
    let client = reqwest::Client::new();

    client
        .post("https://accounts.spotify.com/api/token")
        .header("content-type", "application/x-www-form-urlencoded")
        .header("authorization", format!("Basic {}", gen_basic_auth()))
        .form(&params)
        .send()
        .await?
        .json::<OauthToken>()
        .await
        .map(|token| {
            info!("got refreshed token {:?}", token);
            token
        })
        .map_err(|err| err.into())
}
