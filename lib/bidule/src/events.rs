use rppal::gpio::Level;

#[derive(Debug)]
pub enum Side {
    Left,
    Right,
}

#[derive(Debug)]
pub enum BiduleEvent {
    Gpio(u8, Level),
    ButtonUp,
    ButtonDown,
    ButtonEnter,
    ButtonReturn,
    SwitchMode(Side),
    OauthCallback(String),
    Noop,
}
