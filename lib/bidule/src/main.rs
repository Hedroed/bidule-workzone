mod controls;
mod events;
mod oauth;
mod server;
mod spotify;
mod view_utils;
mod waveshare;
mod app;
mod route_spotify;
mod route_scripts;

use pencil::epdrenderer::convert_to_epd;
use pencil::render_raw;
use rppal::gpio::Gpio;
use rppal::spi::{Bus, Mode, SlaveSelect, Spi};
use std::{error::Error, sync::Arc};
use tokio::sync::mpsc::channel;
use tokio::{
    self,
    sync::{Notify, RwLock},
};
use log::{debug, error, info, warn};

use crate::controls::init_gpio_stream;
use crate::server::serve;
use crate::waveshare::{Update, EPD};

async fn init_edp() -> Result<EPD, Box<dyn Error>> {
    let gpio = Gpio::new()?;
    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 2_000_000, Mode::Mode0)?;
    let mut epd = EPD::new(spi, gpio)?;

    epd.init(Update::FULL)?;
    epd.clear_display(0xff)?;
    info!("Cleared");
    epd.init(Update::PARTIAL)?;
    info!("Init PARTIAL");

    Ok(epd)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .init();

    const VIEW_WIDTH: usize = 250;
    const VIEW_HEIGHT: usize = 122;

    // Init view

    let view_buffer = vec![0u8; VIEW_WIDTH * VIEW_HEIGHT];
    let view_buffer_writer = Arc::new(RwLock::new(view_buffer));
    let view_buffer_reader1 = view_buffer_writer.clone();
    let view_buffer_reader2 = view_buffer_writer.clone();

    let epd_notifier = Arc::new(Notify::new());
    let epd_notifier_clone = epd_notifier.clone();

    if let Some(mut epd) = init_edp().await.ok() {
        tokio::spawn(async move {
            loop {
                epd_notifier_clone.notified().await;
                let buf = {
                    let raw_buf = view_buffer_reader2.read().await;
                    convert_to_epd(raw_buf.as_ref(), VIEW_HEIGHT, VIEW_WIDTH) // width and height MUST be inverted
                }; // allow to release the lock quickly
                epd.display_partial(&buf).expect("unable to display");
            }
        });
    } else {
        warn!("EPD disabled");
    }

    let (gpio_event_sender, mut gpio_event_receiver) = channel(2);

    let event_sender_task_clone = gpio_event_sender.clone();
    let _ = tokio::spawn(async move {
        serve(event_sender_task_clone, view_buffer_reader1).await;
    });

    init_gpio_stream(gpio_event_sender).await?;

    let mut state = crate::app::init().await;

    {
        // first view rendering
        let canvas = crate::app::view(&state).await;

        let mut raw_buf = view_buffer_writer.write().await;
        render_raw(canvas, raw_buf.as_mut()).expect("unable to render image");
        epd_notifier.notify_one();
    }

    debug!("Waiting for gpio events");
    loop {
        if let Some(event) = gpio_event_receiver.recv().await {
            // Update state

            crate::app::update(&mut state, event).await;

            // Render state
            let canvas = crate::app::view(&state).await;

            let mut raw_buf = view_buffer_writer.write().await;
            render_raw(canvas, raw_buf.as_mut()).expect("unable to render image");
            epd_notifier.notify_one();
        } else {
            break;
        }
    }
    warn!("Error when receiving events, all sender are closed");
    Ok(())
}
