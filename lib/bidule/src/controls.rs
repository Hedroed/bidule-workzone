use rppal::gpio::{Error as GpioError, Gpio, InputPin, Level, Trigger};
use std::time::{Duration, Instant};
use std::{collections::HashMap, error::Error};
use tokio::signal;
use tokio::sync::mpsc::Sender;
use log::{debug, info};

use crate::events::{BiduleEvent, Side};

// GPIO
pub const SW1_PORT: u8 = 26; // 37 # ?
pub const SW2_PORT: u8 = 6; // 31 # ?
pub const BT3_PORT: u8 = 21; // 40 # RETURN
pub const BT4_PORT: u8 = 20; // 38 # UP
pub const BT5_PORT: u8 = 19; // 35 # DOWN
pub const BT6_PORT: u8 = 13; // 33 # ENTER

const DEBOUNCE_TIME: Duration = Duration::from_millis(200);

pub async fn trap_exit_signals() -> Result<(), Box<dyn Error>> {
    signal::ctrl_c().await?;
    info!("Received ctrl_c");
    Ok(())
}

fn create_pin(gpio: &Gpio, pin: u8, trigger: Trigger) -> Result<InputPin, GpioError> {
    let mut pin = gpio.get(pin)?.into_input_pullup();
    pin.set_interrupt(trigger)?;
    Ok(pin)
}

pub fn get_event(pin_ref: u8, level: Level) -> BiduleEvent {
    debug!("Gpio event {} : {}", pin_ref, level);

    match pin_ref {
        BT4_PORT => BiduleEvent::ButtonUp,
        BT5_PORT => BiduleEvent::ButtonDown,
        BT3_PORT => BiduleEvent::ButtonReturn,
        BT6_PORT => BiduleEvent::ButtonEnter,
        SW2_PORT => match level {
            Level::Low => BiduleEvent::SwitchMode(Side::Left),
            Level::High => BiduleEvent::SwitchMode(Side::Right),
        },
        _ => BiduleEvent::Gpio(pin_ref, level),
    }
}

struct SwitchState {
    switch1: Level,
    switch2: Level,
}

pub async fn init_gpio_stream(
    gpio_event_sender: Sender<BiduleEvent>,
) -> Result<(), Box<dyn Error>> {
    tokio::task::spawn_blocking(move || -> Result<(), GpioError> {
        let gpio = Gpio::new()?;
        let switch1 = create_pin(&gpio, SW1_PORT, Trigger::Both)?;
        let switch2 = create_pin(&gpio, SW2_PORT, Trigger::Both)?;
        let pins = [
            &switch1,
            &switch2,
            &create_pin(&gpio, BT3_PORT, Trigger::FallingEdge)?,
            &create_pin(&gpio, BT4_PORT, Trigger::FallingEdge)?,
            &create_pin(&gpio, BT5_PORT, Trigger::FallingEdge)?,
            &create_pin(&gpio, BT6_PORT, Trigger::FallingEdge)?,
        ];

        let mut debounce_map = HashMap::<u8, Instant>::new();

        for pin in pins.iter() {
            debounce_map.insert(pin.pin(), Instant::now());
        }

        let mut switch_state = SwitchState {
            switch1: if switch1.is_high() {
                Level::High
            } else {
                Level::Low
            },
            switch2: if switch2.is_high() {
                Level::High
            } else {
                Level::Low
            },
        };

        debug!("Poll gpio interrupts");
        loop {
            let res = gpio.poll_interrupts(&pins, false, None)?;

            if let Some((pin, level)) = res {
                let pin_ref = pin.pin();

                //debounce events : 200ms min between 2 events of the same pin id
                match debounce_map.get(&pin_ref) {
                    Some(prev) => {
                        match &pin_ref {
                            &SW1_PORT => {
                                if switch_state.switch1 == level {
                                    continue;
                                }
                            }
                            &SW2_PORT => {
                                if switch_state.switch2 == level {
                                    continue;
                                }
                            }
                            _ => {}
                        }

                        if prev.elapsed() > DEBOUNCE_TIME {
                            debounce_map.insert(pin_ref, Instant::now());

                            match &pin_ref {
                                &SW1_PORT => {
                                    switch_state.switch1 = level;
                                }
                                &SW2_PORT => {
                                    switch_state.switch2 = level;
                                }
                                _ => {}
                            }

                            let event = get_event(pin_ref, level);

                            if let Err(err) = gpio_event_sender.try_send(event) {
                                // Err if queue full or not listening
                                debug!("Error while sending event {}", err);
                            }
                        }
                    }
                    None => {
                        debug!("Pin id not in debounce map {}", pin_ref);
                    }
                }
            }
        }
    });
    Ok(())
}
