use pencil::Canvas;
use log::{debug, error, info, warn};

use crate::events::{BiduleEvent, Side};
use crate::spotify::Spotify;
use crate::route_spotify::model::{SpotifyState, init as init_spotify};
use crate::route_scripts::model::{ScriptsState, init as init_scripts};

#[derive(Debug)]
pub struct ListRenderState<T> {
    pub level: usize,
    pub items: Vec<T>,
    pub cursor: usize,
}

#[derive(Debug)]
pub enum ViewRoute {
    Scripts(ScriptsState),
    Spotify(SpotifyState),
}

#[derive(Debug)]
pub struct State {
    pub route: ViewRoute,
    pub spotify: Spotify,
}

pub async fn init() -> State {
    let mut spotify = Spotify::new().await;

    if spotify.is_ready() {
        let route = init_spotify(&mut spotify).await;
        State { route, spotify }
    } else {
        let route = init_scripts().await;
        State { route, spotify }
    }
}

pub async fn update(state: &mut State, event: BiduleEvent) {
    match state.route {
        ViewRoute::Spotify(ref mut sub) => {
            crate::route_spotify::update::update(sub, &mut state.spotify, &event).await;
        }
        ViewRoute::Scripts(ref mut sub) => {
            crate::route_scripts::update::update(sub, &event).await;
        }
    };

    match event {
        BiduleEvent::ButtonUp => {
            info!("Click Up");
        },
        BiduleEvent::ButtonDown => {
            info!("Click Down");
        }
        BiduleEvent::ButtonEnter => {
            info!("Click Enter");
        }
        BiduleEvent::ButtonReturn => {
            info!("Click Return");
        }
        BiduleEvent::SwitchMode(Side::Left) => {
            if let ViewRoute::Scripts(_) = state.route {
                return;
            }
            state.route = init_scripts().await;
            info!("Toggle mode to {:?}", state.route);
        }
        BiduleEvent::SwitchMode(Side::Right) => {
            match state.route {
                ViewRoute::Spotify(_) => {}
                ViewRoute::Scripts(_) => {
                    if state.spotify.is_ready() {
                        state.route = init_spotify(&mut state.spotify).await;
                    } else {
                        error!("cannot switch to spotify, no token")
                    }
                }
            };
            info!("Toggle mode to {:?}", state.route);
        }
        BiduleEvent::Gpio(pin, level) => {
            info!("Unknown gpio event {} : {}", pin, level);
        }
        BiduleEvent::OauthCallback(code) => {
            info!("Oauth callback {}", code);

            state.spotify.oauth_authorization_code(&code).await.unwrap();

            if let ViewRoute::Spotify(ref mut sub) = state.route {
                if let Ok(res) = state.spotify.device().await {
                    sub.items = res.devices;
                }
            }

        }
        BiduleEvent::Noop => info!("Noop event"),
    };
}

pub async fn view(state: &State) -> Canvas {
    match state.route {
        ViewRoute::Scripts(ref sub) => crate::route_scripts::view::view(sub),
        ViewRoute::Spotify(ref sub) => crate::route_spotify::view::view(sub),
    }
}
