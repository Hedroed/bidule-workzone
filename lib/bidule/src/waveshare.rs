use rppal::gpio::{Gpio, InputPin, OutputPin};
use rppal::spi::{Bus, Mode, SlaveSelect, Spi};
use std::error::Error;
use std::time::Duration;
use std::{thread, time};
use log::info;

// Let's sleep for 2 seconds:
//thread::sleep_ms(2000);

const RESET_PIN: u8 = 17;
const DC_PIN: u8 = 25;
const CS_PIN: u8 = 8;
const BUSY_PIN: u8 = 24;

const EPD_WIDTH: u32 = 122;
const EPD_HEIGHT: u32 = 250;
const LINEWIDTH: u32 = EPD_WIDTH / 8 + 1;

static LUT_FULL_UPDATE: &'static [u8] = &[
    0x80, 0x60, 0x40, 0x00, 0x00, 0x00, 0x00, // LUT0: BB:     VS 0 ~7
    0x10, 0x60, 0x20, 0x00, 0x00, 0x00, 0x00, // LUT1: BW:     VS 0 ~7
    0x80, 0x60, 0x40, 0x00, 0x00, 0x00, 0x00, // LUT2: WB:     VS 0 ~7
    0x10, 0x60, 0x20, 0x00, 0x00, 0x00, 0x00, // LUT3: WW:     VS 0 ~7
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT4: VCOM:   VS 0 ~7
    0x03, 0x03, 0x00, 0x00, 0x02, // TP0 A~D RP0
    0x09, 0x09, 0x00, 0x00, 0x02, // TP1 A~D RP1
    0x03, 0x03, 0x00, 0x00, 0x02, // TP2 A~D RP2
    0x00, 0x00, 0x00, 0x00, 0x00, // TP3 A~D RP3
    0x00, 0x00, 0x00, 0x00, 0x00, // TP4 A~D RP4
    0x00, 0x00, 0x00, 0x00, 0x00, // TP5 A~D RP5
    0x00, 0x00, 0x00, 0x00, 0x00, // TP6 A~D RP6
    0x15, 0x41, 0xA8, 0x32, 0x30, 0x0A,
];

static LUT_PARTIAL_UPDATE: &'static [u8] = &[
    // 20 bytes
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT0: BB:     VS 0 ~7
    0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT1: BW:     VS 0 ~7
    0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT2: WB:     VS 0 ~7
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT3: WW:     VS 0 ~7
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // LUT4: VCOM:   VS 0 ~7
    0x0A, 0x00, 0x00, 0x00, 0x00, // TP0 A~D RP0
    0x00, 0x00, 0x00, 0x00, 0x00, // TP1 A~D RP1
    0x00, 0x00, 0x00, 0x00, 0x00, // TP2 A~D RP2
    0x00, 0x00, 0x00, 0x00, 0x00, // TP3 A~D RP3
    0x00, 0x00, 0x00, 0x00, 0x00, // TP4 A~D RP4
    0x00, 0x00, 0x00, 0x00, 0x00, // TP5 A~D RP5
    0x00, 0x00, 0x00, 0x00, 0x00, // TP6 A~D RP6
    0x15, 0x41, 0xA8, 0x32, 0x30, 0x0A,
];

const MILLIS_100: Duration = time::Duration::from_millis(100);
const MILLIS_200: Duration = time::Duration::from_millis(200);

#[derive(Debug)]
pub struct EPD {
    spi: Spi,
    reset_pin: OutputPin,
    dc_pin: OutputPin,
    cs_pin: OutputPin,
    busy_pin: InputPin,
}

pub enum Update {
    FULL,
    PARTIAL,
}

impl EPD {
    pub fn new(spi: Spi, gpio: Gpio) -> Result<EPD, Box<dyn Error>> {
        let reset_pin = gpio.get(RESET_PIN)?.into_output();
        let dc_pin = gpio.get(DC_PIN)?.into_output();
        let mut cs_pin = gpio.get(CS_PIN)?.into_output();
        let busy_pin = gpio.get(BUSY_PIN)?.into_input();

        cs_pin.set_low();

        let new_epd = EPD {
            spi,
            reset_pin,
            dc_pin,
            cs_pin,
            busy_pin,
        };
        Ok(new_epd)
    }

    fn wait_until_idle(&self) {
        while self.busy_pin.is_high() {
            thread::sleep(MILLIS_100);
        }
    }

    fn send_command(&mut self, command: u8) -> Result<usize, rppal::spi::Error> {
        self.dc_pin.set_low();
        return self.spi.write(&[command]);
    }

    fn send_data(&mut self, data: u8) -> Result<usize, rppal::spi::Error> {
        self.dc_pin.set_high();
        return self.spi.write(&[data]);
    }

    // Hardware reset
    pub fn reset(&mut self) {
        self.reset_pin.set_high();
        thread::sleep(MILLIS_200);
        self.reset_pin.set_low();
        thread::sleep(MILLIS_200);
        self.reset_pin.set_high();
        thread::sleep(MILLIS_200);
    }

    fn turn_on_display(&mut self) -> Result<(), Box<dyn Error>> {
        self.send_command(0x22)?;
        self.send_data(0xC7)?;
        self.send_command(0x20)?;
        self.wait_until_idle();
        Ok(())
    }

    pub fn init(&mut self, update: Update) -> Result<(), Box<dyn Error>> {
        self.reset();
        match update {
            Update::FULL => {
                self.send_command(0x12)?; // soft reset
                self.wait_until_idle();

                self.send_command(0x74)?; // set analog block control
                self.send_data(0x54)?;
                self.send_command(0x7E)?; // set digital block control
                self.send_data(0x3B)?;

                self.send_command(0x01)?; // Driver output control
                self.send_data(0xF9)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;

                self.send_command(0x11)?; // data entry mode
                self.send_data(0x01)?;

                self.send_command(0x44)?; // set Ram-X address start//end position
                self.send_data(0x00)?;
                self.send_data(0x0F)?; // 0x0C-->(15+1)*8=12?8

                self.send_command(0x45)?; // set Ram-Y address start//end position
                self.send_data(0xF9)?; // 0xF9-->(249+1)=25?0
                self.send_data(0x00)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;

                self.send_command(0x3C)?; // BorderWavefrom
                self.send_data(0x03)?;

                self.send_command(0x2C)?; // VCOM Voltage
                self.send_data(0x55)?;

                self.send_command(0x03)?;
                self.send_data(LUT_FULL_UPDATE[70])?;

                self.send_command(0x04)?;
                self.send_data(LUT_FULL_UPDATE[71])?;
                self.send_data(LUT_FULL_UPDATE[72])?;
                self.send_data(LUT_FULL_UPDATE[73])?;

                self.send_command(0x3A)?; // Dummy Line
                self.send_data(LUT_FULL_UPDATE[74])?;
                self.send_command(0x3B)?; // Gate time
                self.send_data(LUT_FULL_UPDATE[75])?;

                self.send_command(0x32)?;
                for i in 0..70 {
                    self.send_data(LUT_FULL_UPDATE[i])?;
                }

                self.send_command(0x4E)?; // set RAM x address count to 0
                self.send_data(0x00)?;
                self.send_command(0x4F)?; // set RAM y address count to 0X127
                self.send_data(0xF9)?;
                self.send_data(0x00)?;
                self.wait_until_idle();
            }
            Update::PARTIAL => {
                self.send_command(0x2C)?; // VCOM Voltage
                self.send_data(0x26)?;
                self.wait_until_idle();

                self.send_command(0x32)?;
                for i in 0..70 {
                    self.send_data(LUT_PARTIAL_UPDATE[i])?;
                }

                self.send_command(0x37)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;
                self.send_data(0x40)?;
                self.send_data(0x00)?;
                self.send_data(0x00)?;

                self.send_command(0x22)?;
                self.send_data(0xC0)?;
                self.send_command(0x20)?;
                self.wait_until_idle();

                self.send_command(0x3C)?; // BorderWavefrom
                self.send_data(0x01)?;
            }
        }
        return Ok(());
    }

    pub fn display(&mut self, image: &Vec<u8>) -> Result<(), Box<dyn Error>> {
        self.send_command(0x24)?;
        for j in 0..EPD_HEIGHT {
            for i in 0..LINEWIDTH {
                self.send_data(image[(i + j * LINEWIDTH) as usize])?;
            }
        }
        self.turn_on_display()?;
        Ok(())
    }

    pub fn display_partial(&mut self, image: &Vec<u8>) -> Result<(), Box<dyn Error>> {
        self.send_command(0x24)?;
        for j in 0..EPD_HEIGHT {
            for i in 0..LINEWIDTH {
                self.send_data(image[(i + j * LINEWIDTH) as usize])?;
            }
        }
        self.send_command(0x26)?;
        for j in 0..EPD_HEIGHT {
            for i in 0..LINEWIDTH {
                self.send_data(!image[(i + j * LINEWIDTH) as usize])?;
            }
        }
        self.turn_on_display()?;
        Ok(())
    }

    pub fn clear_display(&mut self, color: u8) -> Result<(), Box<dyn Error>> {
        self.send_command(0x24)?;
        for _ in 0..EPD_HEIGHT {
            for _ in 0..LINEWIDTH {
                self.send_data(color)?;
            }
        }
        self.turn_on_display()?;
        Ok(())
    }

    pub fn deep_sleep(&mut self) -> Result<(), Box<dyn Error>> {
        self.send_command(0x22)?; // POWER OFF
        self.send_data(0xC3)?;
        self.send_command(0x20)?;

        self.send_command(0x10)?; // enter deep sleep
        self.send_data(0x01)?;
        thread::sleep(MILLIS_100);
        Ok(())
    }
}

pub fn display_one(filename: String) -> Result<(), Box<dyn Error>> {
    // Configure the SPI peripheral. The 24AA1024 clocks in data on the first
    // rising edge of the clock signal (SPI mode 0). At 3.3 V, clock speeds of up
    // to 10 MHz are supported.
    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 2_000_000, Mode::Mode0)?;
    let gpio = Gpio::new()?;

    let mut epd = EPD::new(spi, gpio)?;

    epd.init(Update::FULL)?;
    epd.clear_display(0xff)?;
    info!("Cleared");

    epd.init(Update::PARTIAL)?;
    info!("Init PARTIAL");

    let buf: Vec<u8> = std::fs::read(filename)?;

    epd.display_partial(&buf)?;
    info!("Display DONE");

    Ok(())
}

pub fn test_display() -> Result<(), Box<dyn Error>> {
    // Configure the SPI peripheral. The 24AA1024 clocks in data on the first
    // rising edge of the clock signal (SPI mode 0). At 3.3 V, clock speeds of up
    // to 10 MHz are supported.
    let spi = Spi::new(Bus::Spi0, SlaveSelect::Ss0, 2_000_000, Mode::Mode0)?;
    let gpio = Gpio::new()?;

    let mut epd = EPD::new(spi, gpio)?;

    epd.init(Update::FULL)?;
    epd.clear_display(0xff)?;
    info!("Cleared");

    epd.init(Update::PARTIAL)?;
    info!("Init PARTIAL");

    let buf1: Vec<u8> = std::fs::read("bidule_rpi1.dat")?;
    let buf2: Vec<u8> = std::fs::read("bidule_rpi2.dat")?;

    let second = time::Duration::from_millis(1000);

    epd.display_partial(&buf1)?;
    info!("Image 1");
    thread::sleep(second);

    epd.display_partial(&buf2)?;
    info!("Image 2");
    thread::sleep(second);

    epd.display_partial(&buf1)?;
    info!("Image 1 again");

    info!("Display DONE");

    Ok(())
}
