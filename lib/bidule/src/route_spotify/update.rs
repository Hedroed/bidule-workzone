use rspotify_model::PlayableItem;
use log::info;

use crate::{events::BiduleEvent, spotify::Spotify};

use super::model::SpotifyState;

pub async fn update(state: &mut SpotifyState, spotify: &mut Spotify, event: &BiduleEvent) {
    match event {
        BiduleEvent::ButtonUp => {
            if state.cursor > 0 {
                state.cursor -= 1;
            };
        }
        BiduleEvent::ButtonDown => {
            if state.items.len() > 0 && state.cursor < state.items.len() - 1 {
                state.cursor += 1;
            };
        }
        BiduleEvent::ButtonEnter => {
            info!("Click Enter");
            if let Some(selected) = state.items.get(state.cursor) {
                if let Some(selected_id) = &selected.id {
                    spotify.transfer_playback(&selected_id, true).await.unwrap();
                }
            };
        }
        BiduleEvent::ButtonReturn => {
            info!("Click Return");
            if let Some(playback) = spotify.current_playback().await.unwrap() {
                if let Some(PlayableItem::Track(track)) = playback.item {
                    state.track = Some(track)
                }
            };
        }
        _ => {}
    };
}
