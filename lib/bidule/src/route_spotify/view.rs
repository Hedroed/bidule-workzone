use invert_rect::InvertRect;
use pencil::*;
use rspotify_model::{Device, FullTrack};
use std::string::String;
use log::{debug, info};

use super::model::SpotifyState;


pub fn view(state: &SpotifyState) -> Canvas {
    debug!("Drawing view");

    // create a canvas to draw on
    let mut canvas = Canvas::new(250, 122);

    let line = components::line::FullLine::new(12, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));

    let text = components::word::Word::new(1, 0, String::from("ip:192.168.234.234"), false);
    canvas.display_list.add(Box::new(text));

    let text = components::word::Word::new(133, 0, String::from("dns:192.168.234.234"), false);
    canvas.display_list.add(Box::new(text));

    view_list(&mut canvas, &state.items[..], state.cursor);

    match state.track {
        Some(ref track) => view_track(&mut canvas, track),
        None => {}
    };

    static BOTTOM_OFFSET: usize = 110;

    let line =
        components::line::FullLine::new(BOTTOM_OFFSET, components::line::Direction::Horizontal);
    canvas.display_list.add(Box::new(line));

    let icon = components::btn_icon::BtnIcon::new(15, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(70, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(125, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));
    let icon = components::btn_icon::BtnIcon::new(180, BOTTOM_OFFSET + 2);
    canvas.display_list.add(Box::new(icon));

    let text = components::word::Word::new(30, BOTTOM_OFFSET + 2, String::from("Return"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(85, BOTTOM_OFFSET + 2, String::from("Up"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(140, BOTTOM_OFFSET + 2, String::from("Down"), false);
    canvas.display_list.add(Box::new(text));
    let text = components::word::Word::new(195, BOTTOM_OFFSET + 2, String::from("Enter"), false);
    canvas.display_list.add(Box::new(text));

    canvas
}

// draw a list of 7 items
fn view_list(canvas: &mut Canvas, items: &[Device], cursor: usize) {
    static OFFSET_TOP: usize = 12;
    const WIDTH: usize = 113; // size of 16 chars
    const HEIGHT: usize = 14;
    const FULL_HEIGHT: usize = 99;

    let line = components::line::Line::new(
        WIDTH,
        components::line::Direction::Vertical,
        OFFSET_TOP,
        FULL_HEIGHT,
    );
    canvas.display_list.add(Box::new(line));

    let line = components::line::Line::new(
        OFFSET_TOP,
        components::line::Direction::Horizontal,
        0,
        WIDTH,
    );
    canvas.display_list.add(Box::new(line));
    let mut x = 0;

    for idx in 0..7 {
        let line = components::line::Line::new(
            OFFSET_TOP + x,
            components::line::Direction::Horizontal,
            0,
            WIDTH,
        );
        canvas.display_list.add(Box::new(line));
        if let Some(item) = items.get(idx) {
            let text = components::word::Word::new(1, OFFSET_TOP + x + 2, item.name.to_string(), true);
            canvas.display_list.add(Box::new(text));
        }
        if cursor == idx {
            let rect = InvertRect::new(0, OFFSET_TOP + x, WIDTH, HEIGHT);
            canvas.display_list.add(Box::new(rect));
        }
        x += HEIGHT;
    }
}

// draw info of the current track
fn view_track(canvas: &mut Canvas, track: &FullTrack) {
    static OFFSET_TOP: usize = 12;
    const HEIGHT: usize = 12;

    let title = components::word::Word::new(118, OFFSET_TOP + 2, track.name.to_string(), false);
    canvas.display_list.add(Box::new(title));
    
    let mut x = 20;
    for artist in &track.artists {
        let text = components::word::Word::new(118, OFFSET_TOP + x + 2, artist.name.to_string(), false);
        canvas.display_list.add(Box::new(text));
        x += HEIGHT;
    }
}
