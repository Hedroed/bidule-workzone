use rspotify_model::{Device, FullTrack};
use log::info;

use crate::{app::ViewRoute, spotify::Spotify};

#[derive(Debug)]
pub struct SpotifyState {
    pub items: Vec<Device>,
    pub cursor: usize,
    pub track: Option<FullTrack>,
}

pub async fn init(spotify: &mut Spotify) -> ViewRoute {

    let mut items = Vec::with_capacity(2);
    
    if let Ok(devices) = spotify.device().await {
        items = devices.devices;
        info!("New list {:?}", items);
    }

    ViewRoute::Spotify(SpotifyState {
        cursor: 0,
        items,
        track: None,
    })
}
