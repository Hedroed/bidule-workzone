from PIL import Image

# Display resolution
EPD_WIDTH = 122
EPD_HEIGHT = 250


def getbuffer(image):
    if EPD_WIDTH % 8 == 0:
        linewidth = EPD_WIDTH // 8
    else:
        linewidth = EPD_WIDTH // 8 + 1

    buf = [0xFF] * (linewidth * EPD_HEIGHT)
    image_monocolor = image.convert('1')
    imwidth, imheight = image_monocolor.size
    pixels = image_monocolor.load()

    if (imwidth == EPD_WIDTH and imheight == EPD_HEIGHT):
        # print("Vertical")
        for y in range(imheight):
            for x in range(imwidth):
                if pixels[x, y] == 0:
                    x = imwidth - x
                    buf[x // 8 + y * linewidth] &= ~(0x80 >> (x % 8))
    elif (imwidth == EPD_HEIGHT and imheight == EPD_WIDTH):
        # print("Horizontal")
        for y in range(imheight):
            for x in range(imwidth):
                newx = y
                newy = EPD_HEIGHT - x - 1
                if pixels[x, y] == 0:
                    newy = imwidth - newy - 1
                    buf[newx // 8 + newy * linewidth] &= ~(0x80 >> (y % 8))
    return buf


if __name__ == "__main__":
    import sys
    m = Image.open(sys.argv[1])

    buf = getbuffer(m)

    with open(sys.argv[2], 'wb') as f:
        f.write(bytes(buf))
