import waveshare
import time


d = waveshare.EPD()

d.init(d.FULL_UPDATE)
d.Clear(0xff)

d.init(d.PART_UPDATE)

with open('bidule_rpi1.dat', 'rb') as f:
    buf = list(f.read())

d.displayPartial(buf)

with open('bidule_rpi2.dat', 'rb') as f:
    buf2 = list(f.read())

time.sleep(1)
d.displayPartial(buf2)

time.sleep(1)
d.displayPartial(buf)

# d.Clear(0xff)

SW1_PORT = 26 # 37 # SENSOR
SW2_PORT = 6 # 31 # MODE
BT3_PORT = 21 # 40 # MODE
BT4_PORT = 20 # 38 # UP
BT5_PORT = 19 # 35 # DOWN
BT6_PORT = 13 # 33 # SET

from gpiozero import Button
from signal import pause

class MyButton():

    def __init__(self, port, **args):
        self.b = Button(port, **args)

        self.b.when_pressed = self._pressed
        self.b.when_released = self._released

        self.lastupdate = 0

        self.when_pressed = None

    def _pressed(self, button):
        if self.when_pressed is not None:
            t = time.time()
            if t - self.lastupdate > 0.05:
                self.lastupdate = t
                self.when_pressed(button)

    def _released(self, button):
        t = time.time()
        self.lastupdate = t


def pressed(channel):
    print("button was pressed", channel)

# btn = MyButton(SW1_PORT, pull_up=True)
# btn.when_pressed = pressed

btn = MyButton(SW2_PORT, pull_up=True)
btn.when_pressed = pressed

btn = MyButton(BT3_PORT, pull_up=True)
btn.when_pressed = pressed

btn = MyButton(BT4_PORT, pull_up=True)
btn.when_pressed = pressed

btn = MyButton(BT5_PORT, pull_up=True)
btn.when_pressed = pressed

btn = MyButton(BT6_PORT, pull_up=True)
btn.when_pressed = pressed



print("waiting")
pause()
