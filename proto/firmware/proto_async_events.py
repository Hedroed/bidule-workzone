import asyncio
import random
import time
import threading


"""
Call event with curl "http://localhost:5000/<event>"
"""

def http_serve(loop, queue):
    from flask import Flask
    import logging
    from werkzeug.serving import run_simple
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    app = Flask(__name__)

    @app.route('/<name>')
    def hello_world(name):
        print("call %s" % name)
        asyncio.run_coroutine_threadsafe(add_event(queue, "call_%s" % name), loop)
        time.sleep(1)
        return 'Hello, World!'

    run_simple('localhost', 5000, app, use_reloader=False)


async def loader(queue):
    while True:
        await queue.put("loader_loop")
        await asyncio.sleep(1)


async def long_async_function(queue, name):
    print("%s :: Running long function" % name)
    await asyncio.sleep(5)

    print("%s :: Put event" % name)
    await queue.put("long_running")

    t = asyncio.create_task(loader(queue))

    try:
        await asyncio.sleep(15)

        print("%s :: Done" % name)
        await queue.put("long_done")
    except asyncio.CancelledError:
        pass
    finally:
        try:
            t.cancel()
            await t
        except asyncio.CancelledError:
            pass


async def add_event(q, name):
    await q.put(name)

async def Controller(queue):
    tasks = set()

    model = {}

    ret = False

    while True:
        # Get a "work item" out of the queue.
        try:
            event = await asyncio.wait_for(queue.get(), 30)
            queue.task_done()
        except asyncio.TimeoutError:
            event = "timeout"

        # for task in tasks:
        #     if task.done() or task.cancelled():
        #         tasks.discard(task)

        print('Controller get event %s' % event)


        # Update

        if event == "call_run" and model.get('run', None) is None:
            t = asyncio.create_task(long_async_function(queue, "Run1"))
            model['run'] = t
            tasks.add(t)

        elif event == "long_done" and model.get('run', None) is not None:
            tasks.discard(model['run'])

            print("Done() %s Cancelled() %s" % (model['run'].done(), model['run'].cancelled()))
            model['run'] = None

            print("RESULTATS")

        elif event == "call_cancel" and model.get('run', None) is not None:
            model['run'].cancel()
            try:
                await model['run']
            except asyncio.CancelledError:
                print("long: cancelled")
            tasks.discard(model['run'])
            model['run'] = None

        elif event == "call_end":
            ret = True

        elif event == "timeout":
            print("No event update")


        # End update

        # View model EZ if new_model

        if ret:
            for task in tasks:
                task.cancel()
            print(f"Cancelling {len(tasks)} outstanding tasks")
            queue.task_done()
            return await asyncio.gather(*tasks, return_exceptions=True)
            



async def main():
    # Create a queue that we will use to store our "workload".
    queue = asyncio.Queue()

    t = threading.Thread(name="WebServerThread", target=http_serve, daemon=True, args=(asyncio.get_running_loop(), queue,))
    t.start()

    task = asyncio.create_task(Controller(queue))

    await task


# Main
asyncio.run(main(), debug=True)
