use rppal::gpio::Gpio;
use rppal::gpio::Trigger;
use signal_hook::{iterator::Signals, SIGINT};

const SW1_PORT: u32 = 26; // 37 # SENSOR
const SW2_PORT: u32 = 6; // 31 # MODE
const BT3_PORT: u32 = 21; // 40 # MODE
const BT4_PORT: u32 = 20; // 38 # UP
const BT5_PORT: u32 = 19; // 35 # DOWN
const BT6_PORT: u32 = 13; // 33 # SET


fn main() {

    let gpio = Gpio::new()?;
    let mut pin = gpio.get(BT6_PORT)?.into_input_pullup();

    pin.set_async_interrupt(FallingEdge, |l: Level| println!("Button {}", l));

    let signals = Signals::new(&[SIGINT])?;
    signals.pending();

}
