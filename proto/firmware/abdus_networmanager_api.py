import adbus
import asyncio
service = adbus.Service(bus='system')
loop = asyncio.get_event_loop()


async def run_print(coro):
    ret = await coro
    print(ret)


proxy = adbus.client.Proxy(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager/Settings',interface='org.freedesktop.NetworkManager.Settings')
loop.run_until_complete(proxy.update())

gs = adbus.client.getset.get_all(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager/Settings',interface='org.freedesktop.NetworkManager.Settings')
loop.run_until_complete(run_print(gs))
{'Connections': ['/org/freedesktop/NetworkManager/Settings/1', '/org/freedesktop/NetworkManager/Settings/2', '/org/freedesktop/NetworkManager/Settings/3', '/org/freedesktop/NetworkManager/Settings/4', '/org/freedesktop/NetworkManager/Settings/5', '/org/freedesktop/NetworkManager/Settings/6', '/org/freedesktop/NetworkManager/Settings/7', '/org/freedesktop/NetworkManager/Settings/8', '/org/freedesktop/NetworkManager/Settings/9', '/org/freedesktop/NetworkManager/Settings/10', '/org/freedesktop/NetworkManager/Settings/11', '/org/freedesktop/NetworkManager/Settings/12', '/org/freedesktop/NetworkManager/Settings/13', '/org/freedesktop/NetworkManager/Settings/14', '/org/freedesktop/NetworkManager/Settings/15', '/org/freedesktop/NetworkManager/Settings/16', '/org/freedesktop/NetworkManager/Settings/17', '/org/freedesktop/NetworkManager/Settings/18', '/org/freedesktop/NetworkManager/Settings/19', '/org/freedesktop/NetworkManager/Settings/20', '/org/freedesktop/NetworkManager/Settings/21', '/org/freedesktop/NetworkManager/Settings/22', '/org/freedesktop/NetworkManager/Settings/23', '/org/freedesktop/NetworkManager/Settings/24', '/org/freedesktop/NetworkManager/Settings/25'], 'Hostname': 'arch-hedroed', 'CanModify': True}
loop.run_until_complete(run_print(proxy.ListConnections()))
['/org/freedesktop/NetworkManager/Settings/1', '/org/freedesktop/NetworkManager/Settings/2', '/org/freedesktop/NetworkManager/Settings/3', '/org/freedesktop/NetworkManager/Settings/4', '/org/freedesktop/NetworkManager/Settings/5', '/org/freedesktop/NetworkManager/Settings/6', '/org/freedesktop/NetworkManager/Settings/7', '/org/freedesktop/NetworkManager/Settings/8', '/org/freedesktop/NetworkManager/Settings/9', '/org/freedesktop/NetworkManager/Settings/10', '/org/freedesktop/NetworkManager/Settings/11', '/org/freedesktop/NetworkManager/Settings/12', '/org/freedesktop/NetworkManager/Settings/13', '/org/freedesktop/NetworkManager/Settings/14', '/org/freedesktop/NetworkManager/Settings/15', '/org/freedesktop/NetworkManager/Settings/16', '/org/freedesktop/NetworkManager/Settings/17', '/org/freedesktop/NetworkManager/Settings/18', '/org/freedesktop/NetworkManager/Settings/19', '/org/freedesktop/NetworkManager/Settings/20', '/org/freedesktop/NetworkManager/Settings/21', '/org/freedesktop/NetworkManager/Settings/22', '/org/freedesktop/NetworkManager/Settings/23', '/org/freedesktop/NetworkManager/Settings/24', '/org/freedesktop/NetworkManager/Settings/25']
coro = adbus.client.call(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager', interface='org.freedesktop.NetworkManager', method='GetDeviceByIpIface', args=('wlp59s0',))
loop.run_until_complete(run_print(coro))
/org/freedesktop/NetworkManager/Devices/2
coro = adbus.client.call(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager/Settings/2', interface='org.freedesktop.NetworkManager.Settings.Connection', method='GetSettings', args=())
loop.run_until_complete(run_print(coro))
{'connection': {'id': 'SFR_3500', 'interface-name': 'wlp59s0', 'permissions': ['user:hedroed:'], 'timestamp': 1581663990, 'type': '802-11-wireless', 'uuid': '651697b4-5e40-476b-8e75-7bcc5be8b58a'}, '802-11-wireless': {'mac-address-blacklist': [], 'mode': 'infrastructure', 'security': '802-11-wireless-security', 'seen-bssids': ['44:CE:7D:74:35:04'], 'ssid': [83, 70, 82, 95, 51, 53, 48, 48]}, '802-11-wireless-security': {'auth-alg': 'open', 'key-mgmt': 'wpa-psk'}, 'ipv4': {'address-data': [], 'addresses': [], 'dns': [], 'dns-search': [], 'method': 'auto', 'route-data': [], 'routes': []}, 'ipv6': {'address-data': [], 'addresses': [], 'dns': [], 'dns-search': [], 'method': 'auto', 'route-data': [], 'routes': []}, 'proxy': {}}
coro = adbus.client.getset.get_all(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager/Devices/2', interface='org.freedesktop.NetworkManager.Device')
loop.run_until_complete(run_print(coro))
{'Udi': '/sys/devices/pci0000:00/0000:00:1c.0/0000:3b:00.0/net/wlp59s0', 'Path': 'pci-0000:3b:00.0', 'Interface': 'wlp59s0', 'IpInterface': 'wlp59s0', 'Driver': 'iwlwifi', 'DriverVersion': '5.7.9-arch1-1', 'FirmwareVersion': '53.c31ac674.0 cc-a0-53.ucode', 'Capabilities': 1, 'Ip4Address': 234989760, 'State': 100, 'StateReason': (100, 0), 'ActiveConnection': '/org/freedesktop/NetworkManager/ActiveConnection/11', 'Ip4Config': '/org/freedesktop/NetworkManager/IP4Config/35', 'Dhcp4Config': '/org/freedesktop/NetworkManager/DHCP4Config/6', 'Ip6Config': '/org/freedesktop/NetworkManager/IP6Config/50', 'Dhcp6Config': '/org/freedesktop/NetworkManager/DHCP6Config/5', 'Managed': True, 'Autoconnect': True, 'FirmwareMissing': False, 'NmPluginMissing': False, 'DeviceType': 2, 'AvailableConnections': ['/org/freedesktop/NetworkManager/Settings/5'], 'PhysicalPortId': '', 'Mtu': 1500, 'Metered': 4, 'LldpNeighbors': [], 'Real': True, 'Ip4Connectivity': 4, 'Ip6Connectivity': 4, 'InterfaceFlags': 65539, 'HwAddress': '50:EB:71:8D:83:D5'}
nm = adbus.client.Proxy(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager',interface='org.freedesktop.NetworkManager')
loop.run_until_complete(run_print(nm.update()))
None
loop.run_until_complete(run_print(nm.ActivateConnection('/org/freedesktop/NetworkManager/Settings/2', '/org/freedesktop/NetworkManager/Devices/2','/')))
/org/freedesktop/NetworkManager/ActiveConnection/12

proxy = adbus.client.Proxy(service, 'org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager/Devices/2',interface='org.freedesktop.NetworkManager.Device')
loop.run_until_complete(proxy.update())

