//! Demonstrates using the `trace` attribute macro to instrument `async`
//! functions.
//!
//! This is based on the [`hello_world`] example from `tokio`. and implements a
//! simple client that opens a TCP stream, writes "hello world\n", and closes
//! the connection.
//!
//! You can test this out by running:
//!
//!     ncat -l 6142
//!
//! And then in another terminal run:
//!
//!     cargo +nightly run --example async_fn
//!
//! [`hello_world`]: https://github.com/tokio-rs/tokio/blob/132e9f1da5965530b63554d7a1c59824c3de4e30/tokio/examples/hello_world.rs
#![deny(rust_2018_idioms)]

use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio::time::{sleep, Duration};

use tracing::{span, Level, instrument};

use tracing_subscriber::fmt;
use tracing_subscriber::prelude::*;

use opentelemetry::exporter::trace::stdout;

use std::{error::Error, io, net::SocketAddr};

#[instrument]
async fn connect(addr: &SocketAddr) -> io::Result<TcpStream> {
    let stream = TcpStream::connect(&addr).await;
    tracing::info!("created stream");
    stream
}

#[instrument]
async fn tempo() {
    tracing::info!("waiting 1s");
    sleep(Duration::from_millis(1000)).await;
}

#[instrument]
async fn write(stream: &mut TcpStream) -> io::Result<usize> {

    tempo().await;

    let result = stream.write(b"hello world\n").await;
    tracing::info!("wrote to stream; success={:?}", result.is_ok());
    result
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let addr = "127.0.0.1:6142".parse()?;

    // Install a new OpenTelemetry trace pipeline
    // let (tracer, _uninstall) = opentelemetry_jaeger::new_pipeline()
    //     .with_service_name("trace-demo")
    //     .install()?;

    // // Create a tracing layer with the configured tracer
    // let telemetry_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    // let honeycomb_config = libhoney::Config {
    //     options: libhoney::client::Options {
    //         api_key: "88b223c3f8820cfd93aeff7eea9db973".to_string(),
    //         dataset: "testrust-dataset".to_string(),
    //         ..libhoney::client::Options::default()
    //     },
    //     transmission_options: libhoney::transmission::Options::default(),
    // };
    // let telemetry_layer = new_honeycomb_telemetry_layer("testrust-service", honeycomb_config);

    let (tracer, _uninstall) = stdout::new_pipeline().with_pretty_print(false).install();
    let telemetry_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    // let fmt_layer = fmt::layer()
    //     .with_target(false);

    // Use the tracing subscriber `Registry`, or any other subscriber that impls `LookupSpan`
    tracing_subscriber::registry()
    // .with(fmt_layer)
    .with(telemetry_layer)
    .init();

    let span = span!(Level::TRACE, "root");
    // Enter the span, returning a guard object.
    let _enter = span.enter();

    // Open a TCP stream to the socket address.
    //
    // Note that this is the Tokio TcpStream, which is fully async.
    let mut stream = connect(&addr).await?;

    write(&mut stream).await?;

    Ok(())
}