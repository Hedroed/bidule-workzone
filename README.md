# Bidule Workzones :robot:

## Milestones

- Canvas lib (minimal drawing lib to create eink screen content)
- png encoding lib (minimal png encoding lib to visualize eink content via http server aka pwnagotchi)
- modern tracing solution : https://docs.rs/opentelemetry/0.10.0/opentelemetry/ | https://github.com/tokio-rs/tracing/blob/master/examples/examples/hyper-echo.rs
- http server (to visualize screen remotely and manage settings) : https://github.com/seanmonstar/warp
- async gpio controller (using blocking rppal interrupt in thread and notify tokio via FutureStream)
- rppal spidev eink controller
- plugins / scripts manager
- Minimal Wifi connect implementation


## PCD

Created using EASY EDA : https://easyeda.com/join?type=project&key=97742ece6e0963dc91639369470b3bf3

### Proto v1

4 buttons :
- up
- down
- back
- enter

2 switches :
- ???
- ???

epaper 2.13" display

#### Firmware

- Rust :
    https://docs.golemparts.com/rppal/0.11.3/rppal/index.html

- Python :
    Spidev & gpiozero

Controllable Access Point : with NetwokManager via dbus api

Wifi connection via https://github.com/balena-io/wifi-connect

### Proto v2

6 buttons :

4 direction arrows (up, left, down, right)
1 back
1 enter

No switches

epaper 2.13" display


### Proto v3

no shield

onlu one touchscreen 

3.5inch LCD Connection via spi
- https://www.waveshare.com/product/raspberry-pi/displays/lcd-oled/3.5inch-rpi-lcd-b.htm
- https://www.waveshare.com/wiki/3.5inch_RPi_LCD_(B)
- 27$


2.8inch Connection via DPI (gpio in alternative mode)
- https://www.waveshare.com/product/raspberry-pi/displays/lcd-oled/2.8inch-dpi-lcd.htm
- 35$

